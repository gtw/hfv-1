//
// hfv.v
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module hfv( // Clock 
	    input clk25,
	    // LED
	    output reg[ 1:0 ] led,
	    // LCD
	    output[ 17:0 ] lcdd, output lcdreset, output lcdsdi, output lcddc,
	    output lcdscl, output lcdcs, output lcdvsync, output lcdhsync,
            output lcddotclk, output lcdde,
	    // AF ADC
	    output reg afadccs, output reg afadcsdi, output reg afadcsclk,
	    input afadcsdo,
	    // Rotary encoders
	    input rot1a, input rot1b, input rot1but,
	    input rot2a, input rot2b, input rot2but,
	    input lbut, input rbut,
	    // Paddles
	    input keyt, input keyr,
	    // Touch paddle sensors
	    output touchosc, input touchdit, input touchdah,
	    // RF ADC
	    output rfadccs, output rfadcsclk, input rfadcsdo0a, 
	    input rfadcsdo1a, input rfadcsdo0b, input rfadcsdo1b,
	    // RF control
	    output reg lo4, output lo40, output rxampbias,
	    // DAC
	    output reg[ 9:0 ] dactx, output reg daccw, output dacclkout,
	    // RTC
	    output reg rtcscl, output reg rtcrst, inout rtcio,
	    // USB host
	    inout usbhost0dp, inout usbhost0dn, 
	    inout usbhost1dp, inout usbhost1dn,
	    // USB device
	    inout usbdevdp, usbdevdn, usbdevpulln,
	    // Audio
	    output reg audiol, output reg audior,
	    // Power amplifier
	    output patr, output rf4, output rf5, output rf6, 
	    output rf7, output rf8 );

    // PLL
    wire clkdacraw; // 100 MHz
    wire clkdspraw; // ~114 MHz
    wire clklo40; // 40 MHz
    wire locked;
    EHXPLLL #( .CLKOP_DIV( 8 ), .CLKOS_DIV( 7 ), .CLKOS2_DIV( 20 ),
	       .FEEDBK_PATH( "CLKOP" ), .CLKFB_DIV( 4 ) )
    pll( .CLKI( clk25 ), .CLKOP( clkdacraw ), .CLKOS( clkdspraw ),
	 .CLKOS2( clklo40 ), .CLKFB( clkdacraw ), .LOCK( locked ) );

    assign lo40 = clklo40;

    reg[ 2:0 ] lo4ctr;
    
    always @( posedge clklo40 )
	if( lo4ctr == 3'b100 ) begin
	    lo4ctr <= 3'b000;
	    lo4 <= ~lo4;
	end else
	    lo4ctr <= lo4ctr + 4'b1;
    
    // Reset logic
    wire osc;
    OSCG oscg( osc );
    
    reg[ 5:0 ] resetctr = 6'b000000;
    
    wire ngrst;
    GSR g( ngrst );
    
    assign ngrst = &resetctr;
    (* nogsr *) always_ff @( posedge osc )
	if( !locked )
	    resetctr <= 1'b0;
	else if( !( &resetctr ) )
	    resetctr <= resetctr + 1'b1;

    reg reset = 1'b1;

    always_ff @( posedge clk )
	reset <= 1'b0;
    
    wire clk;
    DCCA dcca25( clk25, ngrst, clk );

    wire clkdac;
    DCCA dccadac( clkdacraw, ngrst, clkdac );
    
    wire clkdsp;
    DCCA dccadsp( clkdspraw, ngrst, clkdsp );
    
    // Bus
    wire[ 31:2 ] addr;
    wire we;
    wire[ 3:0 ] wsel;
    wire[ 31:0 ] wdata;   

    // LEDs
    initial led = 2'b11;

    // AF ADC
    initial afadccs = 1'b1;
    initial afadcsclk = 1'b0;
    initial afadcsdi = 1'b0;

    // Rotary encoders
    reg[ 7:0 ] rot1 = 8'h00;
    reg[ 7:0 ] rot2 = 8'h00;

    // RTC
    wire rtcrx;
    reg rtctx = 1'b0;
    reg rtcz = 1'b1;
    initial rtcscl = 1'b0;
    initial rtcrst = 1'b0;
    BBPU rtc( .I( rtctx ), .T( rtcz ), .O( rtcrx ), .B( rtcio ) );

    // DTR
    wire[ 5:0 ] dtrtemp;
    dtr dtr( clk, dtrtemp );
    
    // USB keyboard
    wire[ 63:0 ] usb0rpt;
    wire[ 63:0 ] usb1rpt;
    wire usb0int;
    wire usb1int;
    wire usb0ack;
    wire usb1ack;

    // USB device
    wire[ 7:0 ] usbdevin;
    wire usbdevint;
    wire usbdevack;

    // RF ADC
    wire[ 31:0 ] rfadcpwr;
    wire[ 31:0 ] rfadcchanpwr;
    reg[ 3:0 ] rfgain;
    reg[ 4:0 ] afgain;
    reg[ 7:0 ] rxbias;
       
    wire tr;
    
    // CPU writes
    always_ff @( posedge clk )
	if( we )
	    if( addr[ 31:28 ] == 4'hA ) begin // RTC
		rtcz <= wdata[ 3 ];
		rtcrst <= wdata[ 2 ];
		rtcscl <= wdata[ 1 ];
		rtctx <= wdata[ 0 ];
            end else if( addr[ 31:28 ] == 4'hC ) // panel
		/* led <= ~wdata[ 1:0 ] */;
	    else if( addr[ 31:28 ] == 4'hE ) begin // AF ADC
		afadccs <= wdata[ 2 ];
		afadcsclk <= wdata[ 1 ];
		afadcsdi <= wdata[ 0 ];		
	    end else if( addr[ 31:28 ] == 4'hF ) begin // RF ADC
		if( wsel[ 2 ] )
		    rfgain <= wdata[ 19:16 ];
		if( wsel[ 1 ] )
		    rxbias <= wdata[ 15:8 ];
		if( wsel[ 0 ] )
		    afgain <= wdata[ 4:0 ];
	    end

    always_comb
	led = { !tr, tr };

    assign patr = tr;
    assign rf4 = 1'b0;
    assign rf5 = 1'b0;
    assign rf6 = 1'b0;
    assign rf7 = 1'b0;
    assign rf8 = 1'b0;
    
    assign usb0ack = we && addr[ 31:28 ] == 4'hB && wdata[ 0 ];
    assign usb1ack = we && addr[ 31:28 ] == 4'hB && wdata[ 1 ];
    assign usbdevack = addr[ 31:28 ] == 4'hB && addr[ 4:2 ] == 3'b100;
    
    // CPU reads
    wire[ 31:0 ] dacrdata;
    reg[ 31:0 ] rdata;
    always_comb
	if( addr[ 31:28 ] == 4'h9 ) // DAC
	    rdata = dacrdata[ 31:0 ];
	else if( addr[ 31:28 ] == 4'hA ) // DTR, RTC
	    rdata = { 11'd0, dtrtemp, 15'd0, rtcrx };
	else if( addr[ 31:28 ] == 4'hB ) // USB
	    unique case( addr[ 4:2 ] )
	    3'b000:
		rdata = usb0rpt[ 31:0 ];
	    3'b001:
		rdata = usb0rpt[ 63:32 ];
	    3'b010:
		rdata = usb1rpt[ 31:0 ];
	    3'b011:
		rdata = usb1rpt[ 63:32 ];
	    3'b100:
		rdata = { 23'd0, usbdevint, usbdevin[ 7:0 ] };
	    default:
		rdata = { 29'b0, usbdevint, usb1int, usb0int };
	    endcase
	else if( addr[ 31:28 ] == 4'hC ) // panel
	    rdata = { 12'd0, rbut, lbut, rot1but, rot2but, 
		      rot1[ 7:0 ], rot2[ 7:0 ] };
	else if( addr[ 31:28 ] == 4'hE ) // AF ADC
	    rdata = { 31'd0, afadcsdo };
	else if( addr[ 31:28 ] == 4'hF ) // RF ADC
	    rdata = addr[ 2 ] ? rfadcchanpwr[ 31:0 ] : rfadcpwr[ 31:0 ];
	else
	    rdata = 32'hXXXXXXXX;

    // Rotary encoders
    reg rot1a0 = 1'b1;
    reg rot1b0 = 1'b1;
    reg rot2a0 = 1'b1;
    reg rot2b0 = 1'b1;
    
    always_ff @( posedge clk ) begin
	rot1a0 <= rot1a;
	rot1b0 <= rot1b;
	if( rot1a && !rot1a0 && !rot1b )
	    rot1 <= rot1 + 8'h01;
	if( rot1b && !rot1b0 && !rot1a )
	    rot1 <= rot1 - 8'h01;
	
	rot2a0 <= rot2a;
	rot2b0 <= rot2b;
	if( rot2a && !rot2a0 && !rot2b )
	    rot2 <= rot2 + 8'h01;
	if( rot2b && !rot2b0 && !rot2a )
	    rot2 <= rot2 - 8'h01;
    end

    lcd lcd( clk, lcdd, lcdreset, lcdsdi, lcddc, lcdscl, lcdcs, lcdvsync, 
	     lcdhsync, lcddotclk, lcdde, addr, wdata, wsel, we );
    
    cpu cpu( clk, reset, addr, wdata, wsel, we, rdata, 1'b1 );

    wire sidetone;
    
    dac dac( clk, clkdac, keyt, keyr, touchosc, touchdit, touchdah,
	     tr, dactx[ 9:0 ], daccw, dacclkout,
	     addr[ 3:2 ], wdata[ 31:0 ], we && addr[ 31:28 ] == 4'h9,
	     dacrdata[ 31:0 ], sidetone );

    usbkbd usbkbd0( clk, usb0rpt[ 63:0 ], usb0int, usb0ack, 
		    usbhost0dp, usbhost0dn );
    usbkbd usbkbd1( clk, usb1rpt[ 63:0 ], usb1int, usb1ack, 
		    usbhost1dp, usbhost1dn );
    usbdev usbdev( clk, usbdevin[ 7:0 ], usbdevint, usbdevack,
		   clk, usbdevdp, usbdevdn, usbdevpulln );

    // ADC
    wire[ 17:0 ] rfin;
    adc adc( clk, clkdsp, rfadccs, rfadcsclk, rfadcsdo0a, rfadcsdo1a,
	     rxampbias, rfgain, afgain, rxbias, rfin, rfadcpwr, rfadcchanpwr );

    reg[ 17:0 ] sigma;
    reg[ 17:0 ] pcm;
    reg out;
    
    always_ff @( posedge clk ) begin
	reg[ 18:0 ] newsigma;

	newsigma = sigma + pcm;
	
	sigma <= newsigma[ 17:0 ];
	out <= newsigma[ 18 ];
	pcm <= rfin + 18'h20000;
    end

    always_comb
	// FIXME tr isn't quite the right conditional... also want sidetone
	// for local control
	if( tr ) begin
	    audiol = sidetone;
	    audior = sidetone;
	end else begin
	    audiol = out;
	    audior = out;
	end
endmodule
