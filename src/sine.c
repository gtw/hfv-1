#include <math.h>
#include <stdio.h>

extern int main( void ) {

    int i;

    for( i = 0; i < 0x800; i++ )
	printf( "%03lX\n", lrint( 0x1FF * sin( i * M_PI / 0x1000 ) ) );
    
    return 0;
}
