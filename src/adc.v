//
// adc.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module adc( input clk25, input clkdsp, output rfadccs, output rfadcsclk,
            input rfadcsdo0a, input rfadcsdo1a, output reg rxampbias,
	    input[ 3:0 ] rfgain, input[ 4:0 ] afgain, input[ 7:0 ] rxbias,
	    output[ 17:0 ] samp,
	    output reg[ 31:0 ] rfpwr, output reg[ 31:0 ] chanpwr );
    
    wire[ 15:0 ] rfin;
    wire sampclk;
    adcio io( clk25, rfadccs, rfadcsclk, rfadcsdo0a, rfadcsdo1a, 
	      rfin, sampclk );

    // RF amp
    wire[ 15:0 ] preamprf;
    wire preampclk;
    amp preamp( clk25, rfin, preamprf, rfgain, sampclk, preampclk );
	       
    // Clock sync
    reg[ 15:0 ] rfsync1;
    reg[ 15:0 ] rfsync;
    reg sampclksync1;
    reg sampclksync;
    reg sampclkprev;
    
    always_ff @( posedge clkdsp ) begin
	rfsync1 <= preamprf;
	rfsync <= rfsync1;
	sampclksync1 <= preampclk;
	sampclksync <= sampclksync1;
	sampclkprev <= sampclksync;
    end

    wire fastclksamp;
    assign fastclksamp = sampclksync && !sampclkprev;
    
    // Resamplers
    wire[ 17:0 ] samp0;
    wire out0;
    wire[ 17:0 ] samp1;
    wire out1;
    wire[ 17:0 ] samp2;
    wire out2;
    wire[ 17:0 ] samp3;
    wire out3;
    wire[ 17:0 ] samp4;
    wire out4;
    wire[ 17:0 ] samp5;
    wire out5;
    resample resample0( clkdsp, { rfsync[ 15:0 ], rfsync[ 15:14 ] }, 
			fastclksamp, samp0, out0 );
    resample resample1( clkdsp, samp0, out0, samp1, out1 );
    resample resample2( clkdsp, samp1, out1, samp2, out2 );
    resample resample3( clkdsp, samp2, out2, samp3, out3 );
    resample resample4( clkdsp, samp3, out3, samp4, out4 );
    resample resample5( clkdsp, samp4, out4, samp5, out5 );

    // Dejitter
    reg[ 17:0 ] djbuf[ 0:7 ];
    reg[ 2:0 ] nextread = 3'h0;
    reg[ 2:0 ] nextwrite = 3'h4;
    reg[ 13:0 ] delay;
    
    always_ff @( posedge clkdsp )
	if( out5 ) begin
	    djbuf[ nextwrite ] <= samp5;
	    nextwrite <= nextwrite + 1;
	end

    reg[ 17:0 ] rawafsamp;
    
    always_ff @( posedge clk25 )
	if( delay + 4 > 15625 ) begin
	    delay <= delay + 4 - 15625;
	    rawafsamp <= djbuf[ nextread ];
	    nextread <= nextread + 1;
//	    ready <= 1;
	end else begin
	    delay <= delay + 4;
//	    ready <= 0;
	end

    // RX amp bias
    reg[ 7:0 ] biassigma;
    always_ff @( posedge clk25 ) begin
	biassigma[ 7:0 ] <= biassigma[ 7:0 ] + rxbias[ 7:0 ];
	rxampbias <= biassigma[ 7:0 ] + rxbias[ 7:0 ] >= 9'h100;
    end
    
    // Power
    reg[ 14:0 ] rfabs;
    reg[ 16:0 ] chanabs;
    wire rfasqrdy;
    wire chanasqrdy;
    wire[ 29:0 ] rfasq;
    wire[ 33:0 ] chanasq;
    reg[ 5:0 ] slowclksamp;
    
    always_ff @( posedge clk25 )
	if( sampclk ) begin
	    rfabs <= rfin[ 15 ] ? ~rfin[ 14:0 ] : rfin[ 14:0 ];
	    chanabs <= rawafsamp[ 17 ] ? ~rawafsamp[ 16:0 ] : rawafsamp[ 16:0 ];
	end

    wire afgainrdy;
    amp #( 18 ) afamp( clk25, rawafsamp, samp, afgain, 1'b1, afgainrdy );
    
    always_ff @( posedge clkdsp )
	slowclksamp <= slowclksamp + 1'b1;
    
    asq #( 15 ) rfsq( clkdsp, rfabs, fastclksamp, rfasq, rfasqrdy );
    asq #( 17 ) chansq( clkdsp, chanabs, slowclksamp[ 5 ],
			chanasq, chanasqrdy );

    reg[ 17:0 ] intctr;
    reg[ 47:0 ] rfpwracc;
    reg[ 51:0 ] chanpwracc;
    
    always_ff @( posedge clk25 )
	if( sampclk ) begin
	    intctr <= intctr + 18'b1;
	    if( intctr == 18'd0 ) begin
		rfpwracc <= 48'd0;
		chanpwracc <= 52'd0;
		rfpwr <= rfpwracc[ 47:16 ];
		chanpwr <= chanpwracc[ 51:20 ];
	    end else begin
		rfpwracc <= rfpwracc + rfasq;
		chanpwracc <= chanpwracc + chanasq;
	    end
	end
endmodule

module adcio( input clk25, output rfadccs, output rfadcsclk,
              input rfadcsdo0a, input rfadcsdo1a, 
	      output reg[ 15:0 ] samp, output reg sampclk );
    
    reg[ 3:0 ] phase;
    assign rfadccs = phase[ 3 ];
    assign rfadcsclk = phase > 4'h7 || !phase || !clk25;
    reg[ 15:0 ] rfin;
    
    always_ff @( posedge clk25 ) begin
        phase <= phase + 4'd1;

        if( !phase[ 3 ] ) 
            rfin[ 15:0 ] <= { rfin[ 13:0 ], rfadcsdo1a, rfadcsdo0a };

	if( phase == 4'd8 )
	    samp <= rfin;

	sampclk <= phase == 4'd0;
    end    
endmodule
