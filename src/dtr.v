//
// dtr.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module dtr( input clk, output reg[ 5:0 ] temp );

    reg start = 1'b0;
    wire[ 7:0 ] sample;
    
    DTR dtr( start, sample[ 7 ], sample[ 6 ], sample[ 5 ], sample[ 4 ],
	     sample[ 3 ], sample[ 2 ], sample[ 1 ], sample[ 0 ] );

    enum {
	PULSE, WAIT, SKIP
    } state;

    initial state = SKIP;
    
    always_ff @( posedge clk )
	if( state == PULSE ) begin
	    start <= 1'b1;
	    state <= WAIT;
	end else if( state == WAIT ) begin
	    start <= 1'b0;
	    if( sample[ 7 ] )
		state <= SKIP;
	    else
		state <= WAIT;
	end else begin
	    start <= 1'b0;
	    temp <= sample[ 5:0 ];
	    state <= PULSE;
	end
endmodule
