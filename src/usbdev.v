//
// usbdev.v
//
// Copyright 2020, 2021, 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module usbdev( input busclk, output[ 7:0 ] rdat, output interrupt,
	       input intack,
	       input clk, inout dp, inout dn, output pulln );
    // frequency of clk (Hz): need the absolute frequency specified to derive
    // USB timing parameters from it
    parameter CLK_FREQ = 25000000;

    // clk periods from transition to sample point: 333.33 ns
    localparam RX_EYE = CLK_FREQ / 3000000;
    // clk periods per symbol: 666.67 ns
    localparam TX_TICK = CLK_FREQ / 1500000;
    localparam TX_LASTTICK = TX_TICK - 1;

    // ~10 ms timer:
    localparam TIMER_SIZE = CLK_FREQ < 4000000 ? 14 :
			    CLK_FREQ < 8000000 ? 15 :
			    CLK_FREQ < 16000000 ? 16 :
			    CLK_FREQ < 32000000 ? 17 :
			    CLK_FREQ < 64000000 ? 18 :
			    CLK_FREQ < 128000000 ? 19 : 20;

    reg[ TIMER_SIZE:0 ] timectr = 0;

    always @( posedge clk )
	timectr <= timectr + 1;

    wire timesig;
    assign timesig = &timectr[ TIMER_SIZE:0 ];

    reg transmit = 1'b0;   
    reg dptx;
    reg dntx;   
    wire dprx_unsync;
    wire dnrx_unsync;
    reg dprx;
    reg dnrx;
    reg dpprev = 1'b0;

    BB bbdp( dptx, !transmit, dprx_unsync, dp );
    BB bbdn( dntx, !transmit, dnrx_unsync, dn );   

    always @( posedge clk ) begin
	dprx <= dprx_unsync;
	dnrx <= dnrx_unsync;
	dpprev <= dprx;
    end

    // initstate:
    localparam
	INITWAIT = 3'd0, // 10 ms timeout -> RESET
	RESET = 3'd1, // 10 ms timeout -> RESETWAIT
	READY = 3'd2; // 10 ms timeout -> INITWAIT
    reg[ 2:0 ] initstate = INITWAIT;
    reg watchdog;
    
    always @( posedge clk ) begin
	if( initstate == INITWAIT && timesig )
	    initstate <= RESET;
	else if( initstate == RESET && timesig )
	    initstate <= READY;
    end

    // low speed device: pull D- high, except during reset
    assign pulln = initstate != RESET;
    
    // shared between TX/RX
    localparam
	IDLE = 3'd0,
	RX = 3'd2,
	RX_EOP = 3'd3,
	TX = 3'd4,
	TX_EOP = 3'd5,
	TX_FORCE = 3'd6,
	TX_EOP2 = 3'd7;
    reg[ 2:0 ] state = IDLE;
    reg[ 2:0 ] nextstate;

    localparam
	NONE = 2'd0,
	INDESCDEV = 2'd1, // GET_DESCRIPTOR device
	INDESCCONF = 2'd2; // GET_DESCRIPTOR configuration
    reg[ 1:0 ] transfer = NONE;
    reg datapacket;
    reg[ 31:0 ] rxword;   
    reg rxoldlev;   
    reg[ 2:0 ] rxstuffctr = 3'b0;
    reg[ 7:0 ] rxbitctr;
    reg rxmustack;
    reg rxinreq;
    reg[ 2:0 ] rxacks;
    reg rxrecvd;
    reg[ 5:0 ] rxphase;
    reg[ 7:0 ] usbfifodat;
    reg usbfifowe;
    
    wire rxeye;
    assign rxeye = rxphase == RX_EYE;

    wire rxtrans;
    assign rxtrans = dprx != dpprev;
   
    wire rxrecv;
    assign rxrecv = rxeye && rxstuffctr < 3'd6 && ( dprx || dnrx );   
	    
    // TX
    reg[ 6:0 ] txlen;      
    reg[ 7:0 ] txcount;      
    reg[ 87:0 ] txbuf;   
    reg[ 5:0 ] txtick = 6'b0;
    reg[ 2:0 ] txstuffctr = 3'b0;
    wire txtrans;
    assign txtrans = txstuffctr == 3'd6 ||
		     ( txcount[ 7 ] && txcount[ 2:0 ] != 3'b111 ) ||
		     ( !txcount[ 7 ] && !txbuf[ 0 ] );
    
    // Main state machine
    always @*
	if( state == RX )
	    // FIXME should check CRC
	    nextstate = rxeye && !dprx && !dnrx ? RX_EOP : RX;
	else if( state == RX_EOP )
	    if( dnrx )
		nextstate = ( rxmustack || rxinreq ) ? TX : IDLE;
	    else
		nextstate = RX_EOP;
	else if( state == TX )
	    nextstate = txtick == TX_LASTTICK && txcount == txlen &&
			 txstuffctr < 3'd6 ? TX_EOP : TX;
	else if( state == TX_EOP )
	    nextstate = txtick == TX_LASTTICK ? TX_EOP2 : TX_EOP;
	else if( state == TX_FORCE )
	    nextstate = initstate == RESET ? TX_FORCE : RX_EOP;
	else if( state == TX_EOP2 )
	    nextstate = txtick == TX_LASTTICK ? IDLE : TX_EOP2;
	else begin
	    // idle
	    if( dprx )
		nextstate = RX;
	    else if( txlen )
		nextstate = TX;
	    else if( initstate == RESET )
		nextstate = TX_FORCE;
	    else
		nextstate = state;	
	end
   
    always @( posedge clk ) begin
	// RX phase: reset at transition, then increment
	if( state == RX )
	    rxphase <= ( rxtrans || rxphase == TX_TICK ) ? 6'b0 : 
		       rxphase + 1'b1;
	else
	    rxphase <= 6'b0;      

	// RX bit stuffing counter: increment on consecutive bits
	if( rxeye && rxstuffctr < 3'd6 && rxoldlev == dprx )
	    rxstuffctr <= rxstuffctr + 1'b1;
	else if( rxeye || state != RX )
	    rxstuffctr <= 3'd0;

	// RX previous symbol
	if( rxeye )
	    rxoldlev <= dprx;
	else if( state != RX )
	    rxoldlev <= 1'b0;

	// RX shift register
	if( rxrecv )
	    rxword[ 31:0 ] <= { dprx == rxoldlev, rxword[ 31:1 ] };      

	rxrecvd <= rxrecv;      
      
	// RX bit counter
	if( rxrecv )
	    rxbitctr <= rxbitctr + 1'b1;
	else if( state == RX )
	    rxbitctr <= rxbitctr;
	else
	    rxbitctr <= 8'b11111000;

	if( rxbitctr == 8'b00000010 )
	    rxmustack <= rxword[ 31:30 ] == 2'b11;
	else if( state == RX || state == RX_EOP )
	    rxmustack <= rxmustack;
	else
	    rxmustack <= 1'b0;      

	if( rxbitctr == 8'b00001000 )
	    rxinreq <= rxword[ 31:24 ] == 8'b01101001;
	else if( state == RX || state == RX_EOP )
	    rxinreq <= rxinreq;
	else
	    rxinreq <= 1'b0;

	if( rxbitctr == 8'd8 && rxword[ 31:24 ] == 8'hC3 ) // DATA0
	    transfer <= NONE;
	else if( rxbitctr == 8'd40 && rxword[ 31:0 ] == 32'h01000680 )
	    transfer <= INDESCDEV;
	else if( rxbitctr == 8'd40 && rxword[ 31:0 ] == 32'h02000680 )
	    transfer <= INDESCCONF;

	if( rxbitctr == 8'd8 )
	    datapacket <= rxword[ 31:24 ] == 8'hC3 || // DATA0
			  rxword[ 31:24 ] == 8'h4B; // DATA1
	
	if( rxrecvd && rxbitctr == 8'b00001000 && 
	    rxword[ 31:24 ] == 8'b11010010 ) // ACK
	    rxacks <= rxacks + 3'd1;
	else if( rxbitctr == 8'b00001000 && 
		 rxword[ 31:24 ] == 8'b00101101 ) // SETUP
	    rxacks <= 3'd0;
	
	usbfifodat[ 7:0 ] <= rxword[ 15:8 ];
	usbfifowe <= rxrecvd && datapacket && rxbitctr >= 8'h20 &&
		     rxbitctr[ 2:0 ] == 3'b000;

	state <= nextstate;      

	// TX symbol
	if( state == TX ) begin
	    if( txtick == TX_LASTTICK && nextstate == TX ) begin
		dptx <= txtrans ? ~dptx : dptx;
		dntx <= txtrans ? ~dntx : dntx;
	    end
	end else if( state == TX_EOP || state == TX_EOP2 ) begin
	    // single ended 0 EOP
	    dptx <= 1'b0;
	    dntx <= state == TX_EOP2 &&
		    txtick >= TX_LASTTICK - 1; // before float
	end else if( state == TX_FORCE ) begin
	    // forced TX symbol
	    dptx <= 1'b0;
	    dntx <= 1'b0;
	end else begin
	    // idle J state
	    dptx <= 1'b0;
	    dntx <= 1'b1;	 
	end
	
	// TX bit stuffing counter
	if( state == TX && txtick == TX_LASTTICK )
	    txstuffctr <= txtrans ? 3'b0 : txstuffctr + 3'b1;
	else if( state == TX )
	    txstuffctr <= txstuffctr;
	else
	    txstuffctr <= 3'b0;      

	if( transmit )
	    txtick <= txtick == TX_LASTTICK ? 6'b0 : txtick + 1'b1;
	else
	    txtick <= 6'b0;
      
	transmit <= state[ 2 ];
      
	// TX bit counter
	if( state == IDLE )
	    txcount <= 8'b11111000;
	else if( state == TX && txtick == TX_LASTTICK && txstuffctr < 3'd6 )
	    txcount <= txcount + 1'b1;      

	// TX shift register
	if( initstate == INITWAIT ) begin
	    txlen <= 7'd0;
	end else if( state == RX_EOP && rxmustack ) begin
	    txbuf[ 87:0 ] <= 88'hXXXXXXXXXXXXXXXXXXXXD2; // ACK
	    txlen <= 7'd8;
	end else if( state == RX_EOP && transfer == INDESCDEV && rxinreq && 
		     rxacks == 3'd0 ) begin
	    txbuf[ 87:0 ] <= 88'hF367080000FF020001124B; // DATA1 dev desc
	    txlen <= 7'd88;
	end else if( state == RX_EOP && transfer == INDESCDEV && rxinreq &&
		     rxacks == 3'd1 ) begin
	    txbuf[ 87:0 ] <= 88'h6F1800001000FC311209C3; // DATA0 dev desc
	    txlen <= 7'd88;
	end else if( state == RX_EOP && transfer == INDESCDEV && rxinreq && 
		     rxacks == 3'd2 ) begin
	    txbuf[ 87:0 ] <= 88'hXXXXXXXXXXXX8F3F01004B; // DATA1 dev desc
	    txlen <= 7'd40;
	end else if( state == RX_EOP && transfer == INDESCCONF && rxinreq &&
		     rxacks == 3'd0 ) begin
	    txbuf[ 87:0 ] <= 88'hA70C80000100000902094B; // DATA1 conf desc
	    txlen <= 7'd88;
	end else if( state == RX_EOP && transfer == INDESCCONF && rxinreq &&
		     rxacks == 3'd1 ) begin
	    txbuf[ 87:0 ] <= 88'hXXXXXXXXXXXXXX6AC132C3; // DATA0 conf desc
	    txlen <= 7'd32;
	end else if( state == RX_EOP && rxinreq ) begin
	    txbuf[ 87:0 ] <= 88'hXXXXXXXXXXXXXXXX00004B; // DATA1 empty
	    txlen <= 7'd24;	    
	end else if( state == TX && txtick == TX_LASTTICK && txstuffctr < 3'd6
		 && !txcount[ 7 ] )
	    txbuf[ 87:0 ] <= { 1'bX, txbuf[ 87:1 ] };
	else if( state == TX_EOP )
	    txlen <= 0;
    end

    wire empty;
    
    dcfifo fifo( clk, usbfifodat, usbfifowe, 
		 busclk, rdat, intack, empty, initstate == RESET );

    assign interrupt = !empty;
endmodule
