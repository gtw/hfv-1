//
// hfvctrl.c
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

#include <libusb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static int escape( libusb_device_handle *dev ) {

    libusb_control_transfer( dev, 0x40, 0, 0, 0,
			     (unsigned char *) "\e", 1, 0 );

    return 0;
}

static int ft8( libusb_device_handle *dev, int argc, char *argv[] ) {

    unsigned char msg[ 37 ];
    long freq = atol( argv[ 2 ] );
    int i;

    if( argc != 4 ) {
	fprintf( stderr, "usage: %s -f freq message\n", argv[ 0 ] );
	    
	return 1;
    }
    
    if( freq < 1 || freq > 0xFFFFFFFFFL ) {
	fprintf( stderr, "%s: invalid frequency %s\n", argv[ 0 ],
		 argv[ 2 ] );
	    
	return 1;
    }

    if( strlen( argv[ 3 ] ) < 79 ) {
	fprintf( stderr, "%s: message too short\n", argv[ 0 ] );
	    
	return 1;
    }
	
    msg[ 0 ] = 0xBF;
    msg[ 1 ] = 0xC0 | ( ( freq >> 30 ) & 0x3F );
    msg[ 2 ] = 0xC0 | ( ( freq >> 24 ) & 0x3F );
    msg[ 3 ] = 0xC0 | ( ( freq >> 18 ) & 0x3F );
    msg[ 4 ] = 0xC0 | ( ( freq >> 12 ) & 0x3F );
    msg[ 5 ] = 0xC0 | ( ( freq >> 6 ) & 0x3F );
    msg[ 6 ] = 0xC0 | ( freq & 0x3F );
    for( i = 0; i < 14; i++ )
	msg[ i + 7 ] = 0xC0 | ( argv[ 3 ][ 7 + ( i << 1 ) + 0 ] & 7 ) |
	    ( ( argv[ 3 ][ 7 + ( i << 1 ) + 1 ] & 7 ) << 3 );
    msg[ 21 ] = 0xC0 | ( argv[ 3 ][ 35 ] & 7 ) |
	( ( argv[ 3 ][ 43 ] & 7 ) << 3 );
    for( i = 0; i < 14; i++ )
	msg[ i + 22 ] = 0xC0 | ( argv[ 3 ][ 44 + ( i << 1 ) + 0 ] & 7 ) |
	    ( ( argv[ 3 ][ 44 + ( i << 1 ) + 1 ] & 7 ) << 3 );
	    
    msg[ 36 ] = 0xBE;

    libusb_control_transfer( dev, 0x40, 0, 0, 0, msg, 37, 0 );
	
    return 0;
}

static int set_time( libusb_device_handle *dev ) {

    char msg[ 0x20 ];
    time_t t;

    time( &t );
	
    strftime( msg, sizeof msg, "time %y%m%d%H%M%S\r", gmtime( &t ) );
	
    libusb_control_transfer( dev, 0x40, 0, 0, 0, (unsigned char *) msg,
			     strlen( msg ), 0 );

    return 0;
}

extern int main( int argc, char *argv[] ) {

    libusb_device_handle *dev;
    char buf[ 0x1000 ], *p = buf;
    char **arg, *src;
    
    libusb_init( NULL );
    
    if( !( dev = libusb_open_device_with_vid_pid( NULL, 0x1209, 0xFC31 ) ) ) {
	fprintf( stderr, "%s: could not connect to USB HFV-1 device\n",
		 argv[ 0 ] );
		 
	return 1;
    }

    if( argc > 1 && strlen( argv[ 1 ] ) >= 2 && argv[ 1 ][ 0 ] == '-' )
	switch( argv[ 1 ][ 1 ] ) {
	case 'e':
	    return escape( dev );
	    
	case 'f':
	    return ft8( dev, argc, argv );
	    
	case 't':
	    return set_time( dev );

	default:
	    fprintf( stderr, "%s: invalid option %s\n", argv[ 0 ], argv[ 1 ] );

	    return 1;
	}
    
    arg = argv + 1;
    src = NULL;
    while( p < buf + sizeof buf - 1 ) {
	if( !src ) {
	    if( !*arg )
		break;

	    src = *arg++;
	}

	if( *src )
	    *p++ = *src++;
	else {
	    *p++ = ' ';
	    src = NULL;
	}
    }

    *--p = '\r';

    libusb_control_transfer( dev, 0x40, 0, 0, 0, (unsigned char *) buf,
			     p - buf + 1, 0 );
    
    return 0;
}
