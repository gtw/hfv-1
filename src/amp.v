//
// amp.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module amp #( parameter WIDTH = 16, parameter STAGES = $clog2( WIDTH ) )
    ( input clk, input[ WIDTH-1:0 ] in, output reg[ WIDTH-1:0 ] out,
      input[ STAGES-1:0 ] gain, input validin = 1'b1, output reg validout );

    localparam MSB = 1 << STAGES;
    
    generate
        (* mem2reg *) reg[ WIDTH-1:0 ] stage[ 0:STAGES ];
	reg[ STAGES:0 ] valid = '0;

	always_comb begin
	    stage[ 0 ] = in;
	    valid[ 0 ] = validin;
	end
	
        for( genvar i = 1; i <= STAGES; i = i + 1 )
	    always_ff @( posedge clk ) begin
		if( gain[ STAGES - i ] ) begin
		    if( stage[ i - 1 ][ WIDTH-1:WIDTH-(MSB>>i)-1 ] == '0 ||
			stage[ i - 1 ][ WIDTH-1:WIDTH-(MSB>>i)-1 ] == '1 )
			// won't overflow: shift
			stage[ i ] <= stage[ i - 1 ] << ( MSB >> i );
		    else
			// would overflow: saturate
			stage[ i ] <= { stage[ i - 1 ][ WIDTH - 1 ],
					{ WIDTH - 1 { ~stage[ i - 1 ]
						      [ WIDTH - 1 ] } } };
		end else
		    // no shift
		    stage[ i ] <= stage[ i - 1 ];
		
		valid[ i ] <= valid[ i - 1 ];
	    end

	always_comb begin
	    out = stage[ STAGES ];
	    validout = valid[ STAGES ];
	end
    endgenerate
endmodule

module atten #( parameter WIDTH = 16, parameter STAGES = $clog2( WIDTH ) )
    ( input clk, input[ WIDTH-1:0 ] in, output reg[ WIDTH-1:0 ] out,
      input[ STAGES-1:0 ] loss, input validin = 1'b1, output reg validout );

    localparam MSB = 1 << STAGES;
    
    generate
        (* mem2reg *) reg[ WIDTH-1:0 ] stage[ 0:STAGES ];
	reg[ STAGES:0 ] valid = '0;

	always_comb begin
	    stage[ 0 ] = in;
	    valid[ 0 ] = validin;
	end
	
        for( genvar i = 1; i <= STAGES; i = i + 1 )
	    always_ff @( posedge clk ) begin
		if( loss[ STAGES - i ] )
		    stage[ i ] <= $signed( stage[ i - 1 ] ) >>> ( MSB >> i );
		else
		    stage[ i ] <= stage[ i - 1 ];
		
		valid[ i ] <= valid[ i - 1 ];
	    end

	always_comb begin
	    out = stage[ STAGES ];
	    validout = valid[ STAGES ];
	end
    endgenerate
endmodule
