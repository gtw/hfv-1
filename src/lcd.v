//
// lcd.v
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module lcd( input clk25, 
	    output[ 17:0 ] lcdd, output reg lcdreset, output reg lcdsdi,
	    output reg lcddc, output reg lcdscl, output reg lcdcs,
            output reg lcdvsync, output reg lcdhsync,
            output lcddotclk, output reg lcdde,
	    input[ 31:2 ] addr, input[ 31:0 ] wdata, 
	    input[ 3:0 ] wsel, input we );
    
    initial lcdreset = 1'b1;
    initial lcddc = 1'b0;
    initial lcdcs = 1'b1;
    initial lcdsdi = 1'b0;
		
    reg clk12;
    always_ff @( posedge clk25 )
	clk12 <= ~clk12;

    reg dotclk;
    always_ff @( posedge clk12 )
	dotclk <= ~dotclk;

    reg[ 31:0 ] screen[ 0:511 ];
    always @( posedge clk25 )
	if( we && addr[ 31:28 ] == 4'h8 ) begin
	    if( wsel[ 0 ] )
		screen[ addr[ 10:2 ] ][ 7:0 ] = wdata[ 7:0 ];
	    if( wsel[ 1 ] )
		screen[ addr[ 10:2 ] ][ 15:8 ] = wdata[ 15:8 ];
	    if( wsel[ 2 ] )
		screen[ addr[ 10:2 ] ][ 23:16 ] = wdata[ 23:16 ];
	    if( wsel[ 3 ] )
		screen[ addr[ 10:2 ] ][ 31:24 ] = wdata[ 31:24 ];
	end
    
    always_ff @( posedge clk25 )
	if( we && addr[ 31:28 ] == 4'hD ) begin
	    lcdreset <= wdata[ 4 ];
	    lcdsdi <= wdata[ 3 ];
	    lcddc <= wdata[ 2 ];
	    lcdscl <= wdata[ 1 ];
	    lcdcs <= wdata[ 0 ];
	end

    reg[ 7:0 ] schar;
    reg[ 7:0 ] sattr0;
    always @( posedge clk25 )
	if( v[ 3 ] ) begin
	    schar <= screen[ { h[ 7:4 ], v[ 8:4 ] } ][ 23:16 ];
	    sattr0 <= screen[ { h[ 7:4 ], v[ 8:4 ] } ][ 31:24 ];
	end else begin
	    schar <= screen[ { h[ 7:4 ], v[ 8:4 ] } ][ 7:0 ];
	    sattr0 <= screen[ { h[ 7:4 ], v[ 8:4 ] } ][ 15:8 ];
	end
		
    reg[ 3:0 ] font[ 0:24575 ];
    initial $readmemh( "font.hex", font );

    reg[ 3:0 ] fontpix;
    reg[ 7:0 ] sattr;
    always @( posedge clk25 ) begin
	fontpix <= font[ { schar[ 7:0 ], h[ 3:0 ], v[ 2:0 ] } ];
	sattr[ 7:0 ] <= sattr0[ 7:0 ];
    end

    reg[ 3:0 ] fgr, fgg, fgb, bgr, bgg, bgb, red, grn, blu;
    always_comb begin
	fgr[ 3:0 ] = sattr[ 0 ] ? ( sattr[ 3 ] ? fontpix :
				    { 1'b0, fontpix[ 3:1 ] } ) :
		     sattr[ 3 ] ? { 2'b0, fontpix[ 3:2 ] } : 4'b0000;
	fgg[ 3:0 ] = sattr[ 1 ] ? ( sattr[ 3 ] ? fontpix :
				    { 1'b0, fontpix[ 3:1 ] } ) :
		     sattr[ 3 ] ? { 2'b0, fontpix[ 3:2 ] } : 4'b0000;
	fgb[ 3:0 ] = sattr[ 2 ] ? ( sattr[ 3 ] ? fontpix :
				    { 1'b0, fontpix[ 3:1 ] } ) :
		     sattr[ 3 ] ? { 2'b0, fontpix[ 3:2 ] } : 4'b0000;
	bgr[ 3:0 ] = sattr[ 4 ] ? ( sattr[ 7 ] ? ~fontpix :
				    { 1'b0, ~fontpix[ 3:1 ] } ) :
		     sattr[ 7 ] ? { 2'b0, ~fontpix[ 3:2 ] } : 4'b0000;
	bgg[ 3:0 ] = sattr[ 5 ] ? ( sattr[ 7 ] ? ~fontpix :
				    { 1'b0, ~fontpix[ 3:1 ] } ) :
		     sattr[ 7 ] ? { 2'b0, ~fontpix[ 3:2 ] } : 4'b0000;
	bgb[ 3:0 ] = sattr[ 6 ] ? ( sattr[ 7 ] ? ~fontpix :
				    { 1'b0, ~fontpix[ 3:1 ] } ) :
		     sattr[ 7 ] ? { 2'b0, ~fontpix[ 3:2 ] } : 4'b0000;
	red[ 3:0 ] = fgr[ 3:0 ] + bgr[ 3:0 ];
	grn[ 3:0 ] = fgg[ 3:0 ] + bgg[ 3:0 ];
	blu[ 3:0 ] = fgb[ 3:0 ] + bgb[ 3:0 ];
    end
    assign lcdd = { red[ 3:0 ], red[ 3:2 ], 
		    grn[ 3:0 ], grn[ 3:2 ],
		    blu[ 3:0 ], blu[ 3:2 ] };
    
    reg[ 8:0 ] h;
    reg[ 8:0 ] v;
    
    always_ff @( posedge dotclk )
        if( h == 9'd297 )
            h <= 9'd0;
        else
            h <= h + 1'b1;
    
    always_ff @( posedge dotclk )
	if( h == 9'd297 ) begin
            if( v == 9'd337 )
		v <= 9'd0;
            else
		v <= v + 1'b1;
	end

    always_ff @( posedge dotclk ) begin
	lcdvsync <= ( v < 9'd324 || v >= 9'd328 );
	lcdhsync <= ( h < 9'd250 || h >= 9'd260 );
	lcdde <= ( h < 9'd240 && v < 9'd320 );
    end
    assign lcddotclk = dotclk;
endmodule
