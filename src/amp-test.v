//
// amp-test.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

`ifndef VERBOSE
    `define VERBOSE 0
`endif

module amptest();
    reg clk = 1'b0;
    always #1 clk = ~clk;

    reg[ 9:0 ] x;
    reg xrdy;
    reg[ 3:0 ] gain;
    wire[ 9:0 ] y;
    wire yrdy;
    wire[ 9:0 ] z;
    wire zrdy;
    
    amp #( 10 ) dutamp( clk, x, y, gain, xrdy, yrdy );
    atten #( 10 ) dutatt( clk, x, z, gain, xrdy, zrdy );
    
    integer i, j, resultamp, resultatt;
    initial begin
	$dumpfile( "amp.vcd" );
	$dumpvars( 0, dutamp, dutatt );
	x = 0;
	gain = 0;
	xrdy = 0;
	for( i = -512; i < 512; i++ )
	    for( j = 0; j < 10; j++ ) begin
		resultamp = i << j;
		if( resultamp < -512 )
		    resultamp = -512;
		if( resultamp > 511 )
		    resultamp = 511;
		resultatt = i >>> j;
		x = i;
		gain = j;
		#2 xrdy = 1;
		#2 xrdy = 0;
		while( !yrdy )
		    #1;
		while( !zrdy )
		    #1;
		if( `VERBOSE )
		    $display( "%5d %1d %5d %5d %5d %5d", i, j, resultamp, $signed( y ), resultatt, $signed( z ) );
		
		if( y[ 9:0 ] != resultamp[ 9:0 ] )
		    $fatal( 2, "%5d with gain of %1d is %5d but should be %5d",
			    i, j, $signed( y ), resultamp );
		
		if( z[ 9:0 ] != resultatt[ 9:0 ] )
		    $fatal( 2, "%5d with loss of %1d is %5d but should be %5d",
			    i, j, $signed( z ), resultatt );
	end
	$info( "Success." );
	$finish();
    end

    initial
	#131072 $fatal( 2, "timeout" );
endmodule
