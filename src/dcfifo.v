//
// dcfifo.v
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module dcfifo( wclk, wdat, we, rclk, rdat, re, empty, reset );
    parameter WORD_LEN = 8, ADDR_WID = 11;
    localparam ADDR_MAX = ( 1 << ADDR_WID ) - 1;

    input wclk;
    input[ WORD_LEN - 1:0 ] wdat;
    input we;
    input rclk;
    output reg[ WORD_LEN - 1:0 ] rdat;
    input re;
    output reg empty;
    input reset;
    
    reg[ WORD_LEN - 1:0 ] mem[ 0:ADDR_MAX ];
    wire[ ADDR_WID - 1:0 ] wptr;
    reg[ ADDR_WID - 1:0 ] rptr;
    wire[ ADDR_WID - 1:0 ] rwptr;
    
    always_ff @( posedge wclk )
	if( we )
	    mem[ wptr ][ WORD_LEN - 1:0 ] <= wdat[ WORD_LEN - 1:0 ];

    always_ff @( posedge rclk ) begin
	rdat[ WORD_LEN - 1:0 ] <= mem[ rptr ][ WORD_LEN - 1:0 ];
	empty <= rwptr == rptr;
    end

    always_ff @( posedge rclk or posedge reset )
	if( reset )
	    rptr <= 0;
	else
	    if( re && !empty )
		rptr <= rptr + 1;
    
    syncword #( .WORD_LEN( ADDR_WID ) ) s( wclk, wptr + 1'b1, wptr, we,
					   rclk, rwptr, reset );
endmodule

module syncword( wclk, wnew, wcur, we, rclk, rdat, reset );
    parameter WORD_LEN = 8;

    input wclk;
    input[ WORD_LEN - 1:0 ] wnew;
    output reg[ WORD_LEN - 1:0 ] wcur;
    input we;
    input rclk;
    output reg[ WORD_LEN - 1:0 ] rdat;
    input reset;

    localparam
	IDLE = 2'b00,
	WAIT = 2'b01,
	WRITE = 2'b10,
	FINISH = 2'b11;
    reg[ 1:0 ] state = IDLE;
    reg[ WORD_LEN - 1:0 ] common;
    
    reg req = 1'b0, rreq_unsync = 1'b0, rreq = 1'b0, push = 1'b0;
    reg ack = 1'b0, wack_unsync = 1'b0, wack = 1'b0;

    always_ff @( posedge wclk or posedge reset ) begin
	if( reset ) begin
	    wcur <= '0;
	    common <= '0;
	    wack_unsync <= 1'b0;
	    wack <= 1'b0;
	    push <= 1'b0;
	    req <= 1'b0;	    
	    state <= IDLE;
	end else begin
	    if( we )
		wcur <= wnew;
	    
	    wack_unsync <= ack;
	    wack <= wack_unsync;

	    if( state == WAIT && wack )
		push <= 1'b0;
	    else if( we )
		push <= 1'b1;

	    unique case( state )
	    IDLE:
		if( push ) begin
		    req <= 1'b1;
		    state <= WAIT;
		end
	    WAIT:
		if( wack ) begin
		    common <= we ? wnew : wcur;
		    state <= WRITE;
		end
	    WRITE:
		begin
		    req <= 1'b0;
		    state <= FINISH;
		end
	    FINISH:
		if( !wack )
		    state <= IDLE;
	    endcase
	end
    end

    always_ff @( posedge rclk or posedge reset ) begin
	if( reset ) begin
	    rreq_unsync <= 1'b0;
	    rreq <= 1'b0;

	    ack <= 1'b0;
	    
	    rdat <= '0;
	end else begin
	    rreq_unsync <= req;
	    rreq <= rreq_unsync;

	    ack <= rreq;

	    if( ack && !rreq )
		rdat <= common;
	end
    end    
endmodule
