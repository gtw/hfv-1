//
// resample.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module resample( input clk, input[ 17:0 ] in, input ine, 
		 output reg[ 17:0 ] out, output reg oute );

    reg[ 17:0 ] history[ 0:63 ];
    reg[ 5:0 ] hindex;
    reg[ 17:0 ] hlookup;
    reg[ 17:0 ] coeff[ 0:127 ];
    reg[ 6:0 ] cindex;
    reg[ 17:0 ] clookup;
    reg[ 5:0 ] phase = 6'b0;
    reg[ 2:0 ] decimator = 3'b0;
    reg[ 6:0 ] iter;
    reg[ 38:0 ] accum;
    reg[ 35:0 ] product;
    reg[ 17:0 ] hfactor;
    reg[ 17:0 ] cfactor;
    reg inep;
    
    initial $readmemh( "coeff.hex", coeff );
    
    always_ff @( posedge clk ) begin
	hlookup <= history[ hindex ];
	clookup <= coeff[ cindex ];
	hfactor <= hlookup;
	cfactor <= clookup;
	product <= $signed( hfactor ) * $signed( cfactor );

	inep <= ine;
	if( ine && !inep ) begin
	    iter <= 7'b0;
	    hindex = phase + 6'b1;
	    cindex = decimator[ 0 ];
	    history[ phase ] <= in;
	    phase <= phase + 6'b1;
	    if( decimator == 3'd4 )
		decimator = 3'b0;
	    else
		decimator <= decimator + 3'b1;
	end else begin
	    if( iter < 7'h7F )
		iter <= iter + 7'b1;
	    hindex <= hindex + 6'b1;
	    cindex <= cindex + 7'd2;
	end

	if( iter == 7'b1 )
	    accum <= 38'b0;
	else
	    accum <= $signed( accum ) + $signed( product );

	if( $signed( accum[ 38:19 ] ) < -131072 )
	    out <= 18'h20000;
	else if( $signed( accum[ 38:19 ] ) > 131071 )
	    out <= 18'h1FFFF;
	else
	    out <= accum[ 36:19 ];
	
	oute <= iter == 7'h43 && ( decimator == 3'd1 || decimator == 3'd4 );
    end
endmodule
