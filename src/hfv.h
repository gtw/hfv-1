//
// hfv.h
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

#ifndef _HFV_H_
#define _HFV_H_

extern unsigned short screen[ 0x10 ][ 0x40 ];
extern volatile struct _dac {
    unsigned stat;
    union {
	unsigned freq; // write only
	unsigned touch; // read only
    };
    unsigned lofreq;
} dac;
extern volatile unsigned rtc;
extern volatile struct _kbd {
    unsigned char report[ 3 ][ 8 ];
    unsigned stat;
} kbd;
extern volatile unsigned led;
extern volatile unsigned lcd;
extern volatile unsigned afadc;
extern volatile union _rfadc {
    struct { // write only
	unsigned char afgain;
	unsigned char rxbias;
	unsigned short rfgain;
    };
    struct { // read only
	unsigned rfpwr;
	unsigned chanpwr;
    };
} rfadc;

// DAC stat writes:
#define DAC_KEYER_REQ			0x80000000 // auto-keyer TX enable
#define DAC_KEY_TX			0x40000000 // manual key should TX
#define DAC_KEYER_HOLD			0x20000000 // auto-keyer forced on
#define DAC_ATTEN                       0x0F000000
#define DAC_KEYER_SPEED                 0x00FF0000
#define DAC_KEYER_DIT			0x00000000
#define DAC_KEYER_DAH			0x00000004
#define DAC_KEYER_UP			0x00000003
#define DAC_KEYER_UP_ELEMENT		0x00000000
#define DAC_KEYER_UP_CHARACTER		0x00000001
#define DAC_KEYER_UP_WORD		0x00000002

// DAC stat reads:
#define DAC_STAT_AUTO			0xC0000000
#define DAC_STAT_AUTO_ACTIVE		0x80000000 // auto-keyer is active
#define DAC_STAT_KEY_DOWN		0x40000000 // auto/manual key down
#define DAC_STAT_TX			0x20000000 // T/R switch TX
#define DAC_STAT_MANUAL			0x00000007
#define DAC_STAT_MANUAL_DOWN		0x00000004
#define DAC_STAT_MANUAL_DIT		0x00000000
#define DAC_STAT_MANUAL_DAH		0x00000004
#define DAC_STAT_MANUAL_UP		0x00000003
#define DAC_STAT_MANUAL_UP_ELEMENT	0x00000000
#define DAC_STAT_MANUAL_UP_CHARACTER	0x00000001
#define DAC_STAT_MANUAL_UP_WORD		0x00000002

#define LCD_RESET 0x10
#define LCD_SDI 0x08
#define LCD_DC 0x04
#define LCD_SCL 0x02
#define LCD_CS 0x01

#define RTC_Z 0x08
#define RTC_RST 0x04
#define RTC_SCL 0x02
#define RTC_IO 0x01

#define RTC_SEC 0x00
#define RTC_MIN 0x01
#define RTC_HOUR 0x02
#define RTC_DAY 0x03
#define RTC_MONTH 0x04
#define RTC_YEAR 0x06
#define RTC_CHARGE 0x08
#define RTC_CHARGE_VAL 0xA5 // trickle charge enabled, one diode, 2 K

#define AFADC_CS 0x04
#define AFADC_SCL 0x02
#define AFADC_SDI 0x01

#define AFADC_IN_EXT 0
#define AFADC_IN_XP 1
#define AFADC_IN_YP 2
#define AFADC_IN_XN 3
#define AFADC_IN_YN 4
#define AFADC_IN_MIC 5
#define AFADC_IN_POT_L 6
#define AFADC_IN_POT_R 7

#endif
