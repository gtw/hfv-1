//
// asq.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.
	
// Approximate square

// Compute the approximate square of x, producing result in sq.
// Tolerance <25%.

module asq #( parameter WIDTH = 16 )
    ( input clk, input[ WIDTH-1:0 ] x, input xrdy,
      output reg[ OUTWIDTH-1:0 ] sq, output reg sqrdy );
    
    localparam COUNTWIDTH = $clog2( WIDTH );
    localparam OUTWIDTH = WIDTH * 2;
    
    reg[ WIDTH-1:0 ] in;
    reg[ COUNTWIDTH-1:0 ] count;
    reg copying;
    
    always_ff @( posedge clk )
	if( xrdy ) begin
	    in <= x;
	    count <= WIDTH;
	    copying <= 1'b0;
	    sq <= 'x;
	    sqrdy <= 1'b0;
	end else if( !copying ) begin
	    if( in[ WIDTH - 1 ] ) begin
		// found leading 1: compute two bits
		in <= { in[ WIDTH-3:0 ], 2'b00 };
		count <= count - 1'b1;
		copying <= 1'b1;
		sq <= { sq[ OUTWIDTH-2:0 ], in[ WIDTH - 2 ], ~in[ WIDTH - 2 ] };
		sqrdy <= 1'b0;
	    end else begin
		// trim leading 0: produce 00
		in <= { in[ WIDTH-2:0 ], 1'b0 };
		count <= count - 1'b1;
		copying <= count == 1;
		sq <= { sq[ OUTWIDTH-2:0 ], 2'b00 };
		sqrdy <= 1'b0;
	    end	    
	end else if( count ) begin
	    // after leading 1: copy two bits
	    in <= { in[ WIDTH-3:0 ], 2'b00 };
	    count <= count - 1'b1;
	    copying <= 1'b1;
	    sq <= { sq[ OUTWIDTH-3:0 ], in[ WIDTH-1:WIDTH-2 ] };
	    sqrdy <= 1'b0;
	end else begin
	    // finished
	    in <= in;
	    count <= count;
	    copying <= 1'b1;
	    sq <= sq;
	    sqrdy <= 1'b1;
	end
endmodule
