//
// dcfifo-test.v
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

`ifndef VERBOSE
    `define VERBOSE 0
`endif

module dcfifotest();
    reg wclk = 1'b0;
    always #7 wclk = ~wclk;

    reg rclk = 1'b0;
    always #9 rclk = ~rclk;

    reg[ 7:0 ] wdat;
    reg we;
    wire[ 7:0 ] rdat;
    reg re;
    wire empty;
    reg reset;
    
    dcfifo dut( wclk, wdat, we, rclk, rdat, re, empty, reset );

    function[ 7:0 ] val;
	input integer x;

	val = ( ( x * 8'hB5 ) ^ 8'h73 ) & 8'hFF;
    endfunction
    
    initial begin
	$dumpfile( "dcfifo.vcd" );
	$dumpvars( 0, dut );

	we = 1'b0;
	re = 1'b0;	
	reset = 1'b1;
	
	#40 reset = 1'b0;
	#80;
	
	fork
	    begin // writer
		integer i;

		for( i = 0; i < 2000; i++ ) begin
		    @( posedge wclk ) begin
			if( `VERBOSE )
			    $display( "%10t: write %4d: %02X", $time(), 
				      i, val( i ) );

			wdat <= val( i );
			if( i == 1000 ) begin
			    we <= 1'b0;
			    #60000;
			end
			we <= 1'b1;
		    end
		end
		@( posedge wclk ) we <= 1'b0;
	    end
	    
	    begin // reader
		integer i;		
		for( i = 0; i < 2000; ) begin
		    @( posedge rclk )
			if( empty ) begin
			    re <= 1'b0;
			end else begin
			    if( `VERBOSE )
				$display( "%10t: read %4d: %02X", $time(), 
					  i, rdat );
			    
			    if( rdat != val( i ) )
				$fatal( 2, { "read %02X from %4d, ",
					     "expected %02X" },
					rdat, i, val( i ) );
			    
			    re <= 1'b1;
			    @( posedge rclk ) re <= 1'b0;
			    @( posedge rclk );
			    
			    i++;
			end
		end
	    end
	join
	$info( "Success." );
	$finish();
    end
    
    initial
	#200000 $fatal( 2, "timeout" );    
endmodule
