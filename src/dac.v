//
// dac.v
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

module dac( input sysclk, input clk, input keyt, input keyr, output touchosc,
	    input touchditin, input touchdahin, output tr,
	    output reg[ 9:0 ] tx, output reg cw, output clkout,
	    input[ 1:0 ] addr, input[ 31:0 ] wdata, input we,
	    output[ 31:0 ] rdata, output reg sidetone );
    
    reg[ 19:0 ] resetctr = 20'd1; 
    reg[ 9:0 ] txa;
    reg[ 9:0 ] txb;
    reg dah = 1'b0; // 1=last element was dah (so dit has iambic priority)
    reg[ 20:0 ] trcounter = 21'h180000; // 21'h180000;
    reg[ 25:0 ] keycounter = 26'd0; // element + inter-element timing
    reg[ 26:0 ] waitcounter = 27'd0; // inter-char/word auto spacing
    reg[ 24:0 ] qskcounter = 25'd0; // TR hold time after waitcounter
    reg[ 7:0 ] waitunit = 8'h40; // autokeyer speed
    reg key = 1'b0; // 1=keyer key down
    reg keyhold = 1'b0; // 1=hold autoreq key down (override element length)
    reg[ 3:0 ] loss = 4'd0; // TX attenuation
    
    reg[ 31:0 ] freq = 32'b0;
    reg[ 31:0 ] lofreq = 32'b0;
    reg autoreq = 1'b0; // autokeyer controls TX
    reg autodah;
    reg[ 1:0 ] autospace; // 0=element, 1=character, 2=word
    reg[ 1:0 ] manualspace; // 0=element, 1=character, 2=word
    reg manualtx = 1'b0; // manual key controls TX (otherwise sidetone only)

    ODDRX1F ddr0( clk, 1'b0, txa[ 0 ], txb[ 0 ], tx[ 0 ] );
    ODDRX1F ddr1( clk, 1'b0, txa[ 1 ], txb[ 1 ], tx[ 1 ] );
    ODDRX1F ddr2( clk, 1'b0, txa[ 2 ], txb[ 2 ], tx[ 2 ] );
    ODDRX1F ddr3( clk, 1'b0, txa[ 3 ], txb[ 3 ], tx[ 3 ] );
    ODDRX1F ddr4( clk, 1'b0, txa[ 4 ], txb[ 4 ], tx[ 4 ] );
    ODDRX1F ddr5( clk, 1'b0, txa[ 5 ], txb[ 5 ], tx[ 5 ] );
    ODDRX1F ddr6( clk, 1'b0, txa[ 6 ], txb[ 6 ], tx[ 6 ] );
    ODDRX1F ddr7( clk, 1'b0, txa[ 7 ], txb[ 7 ], tx[ 7 ] );
    ODDRX1F ddr8( clk, 1'b0, txa[ 8 ], txb[ 8 ], tx[ 8 ] );
    ODDRX1F ddr9( clk, 1'b0, txa[ 9 ], txb[ 9 ], tx[ 9 ] );
    
    // Touch paddle sensor
    reg[ 31:0 ] touchstat;
    
    reg[ 9:0 ] touchctr = 10'h200; // start in discharging state
    reg touchdit = 1'b0, touchdah = 1'b0;
    always_ff @( posedge sysclk ) begin
	touchctr <= touchctr + 1'b1;
	if( touchctr == 10'd200 ) begin
	    touchdit <= touchditin;
	    touchdah <= touchdahin;
	end
    end
    assign touchosc = touchctr[ 9 ];

    reg oldtouchdit, oldtouchdah;
    always_ff @( posedge sysclk ) begin
	oldtouchdit <= touchditin;
	oldtouchdah <= touchdahin;
	if( oldtouchdit && !touchditin )
	    touchstat[ 15:0 ] <= { 6'd0, touchctr };
	if( oldtouchdah && !touchdahin )
	    touchstat[ 31:16 ] <= { 6'd0, touchctr };
    end    
    
    always_ff @( posedge sysclk ) begin
	key <= keycounter >= { waitunit, 15'h0000 } && !trcounter;

	if( !tr && ( manualtx || we && addr == 2'b00 && wdata[ 31 ] ) )
	    // TR switch off, need to engage before next element
	    trcounter <= 21'h180000;
	else if( tr && trcounter )
	    // TR switch engage delay
	    trcounter <= trcounter - 1'b1;
	else
	    trcounter <= 21'd0;

	if( autoreq && keyhold )
	    keycounter <= { waitunit, 15'h0000 };
	else if( !keycounter ) begin
	    if( ( dah || keyr && !touchdah ) && !resetctr &&
		( !keyt || touchdit ) ) begin // start dit
		keycounter <= { waitunit, 16'h0000 };
		dah <= 1'b0;
	    end else if( ( !dah || keyt && !touchdit ) && !resetctr &&
			 ( !keyr || touchdah ) ) begin // start dah
		keycounter <= { waitunit, 17'h00000 };
		dah <= 1'b1;
	    end else if( autoreq && !waitcounter )
		keycounter <= autodah ? { waitunit, 17'h00000 } : 
			      { waitunit, 16'h0000 };
	end else if( !trcounter )
	    keycounter <= keycounter - 1'b1;
    
        if( manualtx && key )
	    qskcounter <= { waitunit, 17'h00000 };
	else if( autoreq && key )
	    qskcounter <= 25'h180000;
	else if( qskcounter && !keycounter && !waitcounter )
	    qskcounter <= qskcounter - 1'b1;
    
	if( autoreq ) begin
	    if( key )
		unique case( autospace )
		2'b00:
		    waitcounter <= 27'h0000000;
		2'b01:
		    waitcounter <= { waitunit, 16'h0000 };
		2'b1x:
		    waitcounter <= { waitunit, 17'h00000 } + 
				   { waitunit, 16'h0000 };
		endcase
	    else if( waitcounter && !keycounter )
		waitcounter <= waitcounter - 1'b1;
	end else
	    if( key )
		waitcounter <= { waitunit, 17'h00000 } +
			       { waitunit, 15'h0000 };
	    else begin
		if( waitcounter )
		    waitcounter <= waitcounter - 1'b1;
		
		if( !waitcounter )
		    manualspace <= 2'b10;
		else if( waitcounter < { waitunit, 16'h0000 } +
			 { waitunit, 15'h0000 } )
		    manualspace <= 2'b01;
		else
		    manualspace <= 2'b00;
	    end

	if( we )
	    unique case( addr )
	    2'b00: begin
		manualtx <= wdata[ 30 ];
		keyhold <= wdata[ 29 ];
		loss <= wdata[ 28:24 ];
		waitunit[ 7:0 ] <= wdata[ 23:16 ];
		autodah <= wdata[ 2 ];
		autospace <= wdata[ 1:0 ];
	    end
	    2'b01:
		freq <= wdata[ 31:0 ];
	    2'b10:
		lofreq <= wdata[ 31:0 ];
	    2'b11:
		;
	    endcase

	if( !keyt || !keyr )
	    autoreq <= 1'b0;
	else if( we && addr == 2'b00 ) // FIXME don't turn on autoreq if manual active
	    autoreq <= wdata[ 31 ];
    end

    assign tr = autoreq ||
		manualtx && ( keycounter || waitcounter || qskcounter );

    reg keysync;
    reg[ 8:0 ] sine[ 0:2047 ];
    assign clkout = clk;
    reg[ 8:0 ] lookup0;
    reg[ 8:0 ] amp1;
    reg[ 9:0 ] txatt;
    reg txattrdy;
    reg[ 31:0 ] phase = 32'b0;
    reg invert, invert0, invert1;
    reg[ 10:0 ] index;
    reg[ 18:0 ] envelope = 19'd0;
    reg[ 8:0 ] lolookup0;
    reg[ 31:0 ] lophase = 32'b0;
    reg loinvert, loinvert0;
    reg[ 10:0 ] loindex;
    
    initial begin
	$readmemh( "sine.hex", sine );
	cw <= 1'b1;
    end

    atten #( 10 ) txatten( clk, !trcounter && ( autoreq || manualtx ) ?
			   ( invert1 ? -amp1 : amp1 ) : 10'd0, txatt, loss,
			   1'b1, txattrdy );
    
    always_ff @( posedge clk ) begin
	keysync <= key;
	
	if( keysync && envelope < 19'h7FFFF )
	    envelope <= envelope + 19'b1; // key down
	else if( !keysync && envelope > 19'h00000 )
	    envelope <= envelope - 19'b1; // key up
	
	lookup0 <= sine[ index ];
	amp1 <= ( { 9'b0, lookup0[ 8:0 ] } *
		  { 9'b0, envelope[ 18:10 ] } ) >> 9;
	phase <= phase + freq;

	if( phase[ 30 ] )
	    index <= ~phase[ 29:19 ];
	else
	    index <= phase[ 29:19 ];

	invert <= phase[ 31 ];
	invert0 <= invert;
	invert1 <= invert0;

	lolookup0 <= sine[ loindex ];
	lophase <= lophase + lofreq;
		 
	if( lophase[ 30 ] )
	    loindex <= ~lophase[ 29:19 ];
	else
	    loindex <= lophase[ 29:19 ];

	loinvert <= lophase[ 31 ];
	loinvert0 <= loinvert;	
	
	if( resetctr == 20'd0 ) begin
	    // normal operation
	    txa[ 9:0 ] <= txatt + 10'h200;
	    txb[ 9:0 ] <= loinvert0 ? 10'h200 - lolookup0 : 
			  10'h200 + lolookup0;
	    cw <= 1'b1;
	end else if( resetctr[ 19:18 ] == 2'b10 ) begin
	    txa[ 9:0 ] <= 10'b0110100000;
	    txb[ 9:0 ] <= 10'b0110100000;
	    cw <= 1'b0;
	    resetctr <= resetctr + 20'd1;
	end else begin
	    txa[ 9:0 ] <= 10'b0110100000;
	    txb[ 9:0 ] <= 10'b0110100000;
	    cw <= 1'b1;
	    resetctr <= resetctr + 20'd1;
	end
    end

    // Sidetone generation:
    reg[ 8:0 ] slookup0;
    reg[ 8:0 ] samp1;
    reg[ 25:0 ] sphase = 26'd0;
    reg sinvert, sinvert0, sinvert1;
    reg[ 10:0 ] sindex;
    reg[ 12:0 ] spcm;
    reg[ 12:0 ] sigma;
    
    always_ff @( posedge clk ) begin
	slookup0 <= sine[ sindex ];
	samp1 <= ( { 9'b0, slookup0[ 8:0 ] } *
		   { 9'b0, envelope[ 18:10 ] } ) >> 9;
	sphase <= sphase + 26'd470; // 700 Hz sidetone

	if( sphase[ 24 ] )
	    sindex <= ~sphase[ 23:13 ];
	else
	    sindex <= sphase[ 23:13 ];

	sinvert <= sphase[ 25 ];
	sinvert0 <= sinvert;
	sinvert1 <= sinvert0;
	
	spcm[ 12:0 ] <= sinvert1 ? 13'h1000 - samp1 : 13'h1000 + samp1;

	sigma[ 12:0 ] <= sigma[ 12:0 ] + spcm[ 12:0 ];
	sidetone <= sigma[ 12:0 ] + spcm[ 12:0 ] > 14'h1FFF;
    end
    
    assign rdata = addr[ 0 ] ? touchstat :
		   { autoreq, key, tr, 26'd0, dah, manualspace[ 1:0 ] };
endmodule
