//
// coeff.c
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define FILTER_SIZE 5

#define HALF_TAPS ( ( 1 << FILTER_SIZE ) - 1 )
#define FILTER_TAPS ( HALF_TAPS * 2 + 1 )
#define UPSAMPLE_TAPS ( FILTER_TAPS * 2 + 1 )
#define HISTORY ( FILTER_TAPS + 1 )

static float frac( float x ) {

    return x - floor( x );
}

extern int main( void ) {

    int i;
    int min = 0, max = 0;
    int response;
    float f0 = 0.11285, f1 = 0.17285; // at the upsampled rate
    int coeff[ UPSAMPLE_TAPS ];
	
    coeff[ FILTER_TAPS ] = (int) ( 131072.0 * 16.0 * ( f1 - f0 ) );
    for( i = 1; i <= FILTER_TAPS; i++ )
	coeff[ FILTER_TAPS + i ] = coeff[ FILTER_TAPS - i ] = (int)
	    ( 131072.0 * 16.0 * ( sin( 2.0 * M_PI * frac( i * f1 ) ) /
				( 2.0 * M_PI * i ) -
				sin( 2.0 * M_PI * frac( i * f0 ) ) /
				( 2.0 * M_PI * i ) ) *
	      ( 0.42 - 0.5 * cos( M_PI * ( (float) i / FILTER_TAPS + 1 ) ) +
		0.08 * cos( 2.0 * M_PI * ( (float) i / FILTER_TAPS + 1 ) ) ) );

    for( i = 0; i < UPSAMPLE_TAPS; i++ )
	printf( "%05X\n", coeff[ i ] & 0x3FFFF );

    puts( "00000" );

    for( i = 0; i < UPSAMPLE_TAPS; i++ ) {
	if( coeff[ i ] < min )
	    min = coeff[ i ];
	if( coeff[ i ] > max )
	    max = coeff[ i ];
    }
    fprintf( stderr, "min %d max %d\n", min, max );

    for( i = 0, response = 0; i < UPSAMPLE_TAPS; i += 2 )
	response += abs( coeff[ i ] );
    fprintf( stderr, "max even response %d\n", response );
    
    for( i = 1, response = 0; i < UPSAMPLE_TAPS; i += 2 )
	response += abs( coeff[ i ] );
    fprintf( stderr, "max odd response %d\n", response );
    
    return 0;
}
