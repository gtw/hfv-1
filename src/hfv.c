//
// hfv.c
//
// Copyright 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

#include "hfv.h"

#define NULL ( (void *) 0 )

struct segment {
    unsigned low, high;
    char *label;
} segments[] = {
    {   135700,   137800, "2200 m" },
    {   472000,   479000, "630 m" },
    {  1800000,  2000000, "160 m" },
    {  3500000,  3525000, "80 m Extra CW/data" },
    {  3525000,  3600000, "80 m CW/data" },
    {  3600000,  3700000, "80 m Extra" },
    {  3700000,  3800000, "80 m Advanced" },
    {  3800000,  4000000, "80 m" },
    {  5330500,  5406500, "60 m" },
    {  7000000,  7025000, "40 m Extra CW/data" },
    {  7025000,  7125000, "40 m CW/data" },
    {  7125000,  7175000, "40 m Advanced" },
    {  7175000,  7300000, "40 m" },
    { 10100000, 10150000, "30 m CW/data" },
    { 14000000, 14025000, "20 m Extra CW/data" },
    { 14025000, 14150000, "20 m CW/data" },
    { 14150000, 14175000, "20 m Extra" },
    { 14175000, 14225000, "20 m Advanced" },
    { 14225000, 14350000, "20 m" },
    { 18068000, 18110000, "17 m CW/data" },
    { 18110000, 18168000, "17 m" },
    { 21000000, 21025000, "15 m Extra CW/data" },
    { 21025000, 21200000, "15 m CW/data" },
    { 21200000, 21225000, "15 m Extra" },
    { 21225000, 21275000, "15 m Advanced" },
    { 21275000, 21450000, "15 m" },
    { 24890000, 24930000, "12 m CW/data" },
    { 24930000, 24990000, "12 m" },
    { 28000000, 28300000, "10 m CW/data" },
    { 28300000, 29700000, "10 m" }
};

static char *bindings[ 12 ] = {
    "CQ CQ CQ DE AB1IP AB1IP AB1IP K",
    "TESTING DE AB1IP AB1IP AB1IP",
    "QRL?",
    "CQ CQ QRP CQ DE AB1IP AB1IP AB1IP K",
    "CQ CQ TTF CQ DE AB1IP AB1IP AB1IP K",
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    "",
    "AB1IP"
};

static signed char temperature[ 0x40 ] = {
    -58, -56, -54, -52, -45, -44, -43, -42,
    -41, -40, -39, -38, -37, -36, -30, -20,
    -10, -4, 0, 4, 10, 21, 22, 23,
    24, 25, 26, 27, 28, 29, 40, 50,
    60, 70, 76, 80, 81, 82, 83, 84,
    85, 86, 87, 88, 89, 95, 96, 97,
    98, 99, 100, 101, 102, 103, 104, 105,
    106, 107, 108, 116, 120, 124, 128, 132
};

#define NUM_COMMANDS 22

#define PANEL_NONE 0x00
#define PANEL_L 0x01
#define PANEL_R 0x02
#define PANEL_ROT_L 0x03
#define PANEL_ROT_R 0x04

#define MODE_CW 0
#define MODE_WSPR 1
#define MODE_FT8 2
#define MODE_LSB 3
#define MODE_USB 4

static unsigned oldswitch;
static unsigned freq = 7118300, oldfreq, oldtr;
static unsigned short rfgain = 3, afgain = 0, txatten = 3;
static unsigned waitunit = 87;
static char panelkey;
static int mode;

static unsigned rdcycle( void ) {

    int n;

    asm volatile( "rdcycle %0" : "=r" (n) );

    return n;
}

static void display( int x, int y, char *p, unsigned char attr ) {

    while( *p )
	screen[ y ][ x++ ] = ( attr << 8 ) | *p++;
}

static char hex_digit( unsigned n ) {

    if( n < 0xA )
	return n + '0';
    else
	return n - 0xA + 'A';
}

static void show_hex( int x, int y, unsigned val, int dig,
		      unsigned char attr ) {

    while( dig-- )
	screen[ y ][ x++ ] = ( attr << 8 ) |
	    hex_digit( ( val >> ( dig * 4 ) ) & 0x0F );
}

static void show_dec( int x, int y, unsigned val, int dig,
		      unsigned char attr ) {

    int unit = 1;

    while( --dig )
	unit *= 10;

    while( unit > val ) {
	screen[ y ][ x++ ] = ( attr << 8 ) | ' ';
	unit /= 10;
    }

    if( !val )
	screen[ y ][ x - 1 ] = ( attr << 8 ) | '0';
    
    while( unit ) {
	int n = val / unit;

	screen[ y ][ x++ ] = ( attr << 8 ) + '0' + n;
	val -= n * unit;
	unit /= 10;
    }
}

static void show_signed_dec( int x, int y, int val, int dig,
			     unsigned char attr ) {

    int unit = 1;
    int neg = val < 0;

    if( neg )
	val = -val;

    while( --dig )
	unit *= 10;

    while( unit > val ) {
	screen[ y ][ x++ ] = ( attr << 8 ) | ' ';
	unit /= 10;
    }

    if( !val )
	screen[ y ][ x - 1 ] = ( attr << 8 ) | '0';
    else if( neg )
	screen[ y ][ x - 1 ] = ( attr << 8 ) | '-';
	
    while( unit ) {
	int n = val / unit;

	screen[ y ][ x++ ] = ( attr << 8 ) + '0' + n;
	val -= n * unit;
	unit /= 10;
    }
}

static void show_dec_big( int x, int y, unsigned val, int dig, int zero,
			  unsigned char attr ) {

    int unit = 1;

    while( --dig )
	unit *= 10;

    if( !zero ) {
	while( unit > val ) {
	    screen[ y ][ x ] = ( attr << 8 ) | ' ';
	    screen[ y ][ x + 1 ] = ( attr << 8 ) | ' ';
	    screen[ y + 1 ][ x ] = ( attr << 8 ) | ' ';
	    screen[ y + 1 ][ x + 1 ] = ( attr << 8 ) | ' ';
	    x += 2;
	    unit /= 10;
	}

	if( !val ) {
	    screen[ y ][ x - 2 ] = ( attr << 8 ) | 0x80;
	    screen[ y ][ x - 1 ] = ( attr << 8 ) | 0x81;
	    screen[ y + 1 ][ x - 2 ] = ( attr << 8 ) | 0x82;
	    screen[ y + 1 ][ x - 1 ] = ( attr << 8 ) | 0x83;

	    return;
	}
    }
    
    while( unit ) {
	int n = val / unit;

	screen[ y ][ x ] = ( attr << 8 ) + 0x80 + ( n << 2 );
	screen[ y ][ x + 1 ] = ( attr << 8 ) + 0x81 + ( n << 2 );
	screen[ y + 1 ][ x ] = ( attr << 8 ) + 0x82 + ( n << 2 );
	screen[ y + 1 ][ x + 1 ] = ( attr << 8 ) + 0x83 + ( n << 2 );
	
	val -= n * unit;
	unit /= 10;

	x += 2;
    }
}

static unsigned screencode( unsigned c ) {

    return ( c & 0x80 ) ? ( 0x0A00 | ( c & 0x7F ) ) : ( 0x0200 | c );
}

static void ttychar( unsigned char c ) {

    static unsigned char ring[ 8 ][ 40 ];
    static int top, x;
    
    if( c == '\n' ) {
	int i, j;
	    
	top = ( top + 1 ) & 7;
	x = 0;

	for( i = 0; i < 7; i++ )
	    for( j = 0; j < 40; j++ )
		screen[ 6 + i ][ j ] =
		    screencode( ring[ ( top + i ) & 7 ][ j ] );
		    
	for( i = 0; i < 40; i++ )
	    screen[ 13 ][ i ] = ring[ ( top + 7 ) & 7 ][ i ] = ' ';
    } else {
	// FIXME wrap at col 40
	ring[ ( top + 7 ) & 7 ][ x ] = c;
	screen[ 13 ][ x++ ] = screencode( c );
    }
}

static void tty( char *p ) {

    while( *p )
	ttychar( *p++ );
}

//static void tty_hex( unsigned val, int dig ) {
//
//    while( dig-- )
//	ttychar( hex_digit( ( val >> ( dig * 4 ) ) & 0x0F ) );
//}

static int parse_dec( char *p ) {

    int val;
    
    for( val = 0; *p; p++ )
	if( *p >= '0' && *p <= '9' )
	    val = val * 10 + *p - '0';
	else
	    return -1;

    return val;
}

static void udelay( int us ) {

    int i;
    
    for( i = 12 * us; i; i-- )
	asm volatile( "" );
}

static void delay( int ms ) {

    int i;

    for( i = 12500 * ms; i; i-- )
	asm volatile( "" );
}

static void lcd_cmd( unsigned cmd ) {

    int i;
    
    lcd = LCD_RESET;

    for( i = 0; i < 8; i++ ) {
	lcd = LCD_RESET | ( ( cmd & 0x80 ) ? LCD_SDI : 0 );
	lcd = LCD_RESET | LCD_SCL | ( ( cmd & 0x80 ) ? LCD_SDI : 0 );
	cmd <<= 1;	
    }

    lcd = LCD_RESET | LCD_CS;
}

static void lcd_dat( unsigned dat ) {

    int i;
    
    lcd = LCD_RESET | LCD_DC;

    for( i = 0; i < 8; i++ ) {
	lcd = LCD_RESET | LCD_DC | ( ( dat & 0x80 ) ? LCD_SDI : 0 );
	lcd = LCD_RESET | LCD_DC | LCD_SCL | ( ( dat & 0x80 ) ? LCD_SDI : 0 );
	dat <<= 1;	
    }

    lcd = LCD_RESET | LCD_DC | LCD_CS;
}

static unsigned rtc_read( unsigned addr ) {

    int i;
    int val = 0;
    
    rtc = RTC_RST;
    udelay( 4 );

    for( i = 0; i < 8; i++ ) {
	rtc = RTC_RST | ( addr & 0x01 );
	udelay( 1 );
	rtc = RTC_RST | RTC_SCL | ( addr & 0x01 );
	udelay( 1 );
	addr >>= 1;
    }
    
    for( i = 0; i < 8; i++ ) {
	rtc = RTC_RST | RTC_Z;
	udelay( 1 );
	val = ( val >> 1 ) | ( ( rtc & RTC_IO ) ? 0x80 : 0 );
	rtc = RTC_RST | RTC_Z | RTC_SCL;
	udelay( 1 );
    }

    rtc = RTC_Z;

    return val;
}

static void rtc_write( unsigned addr, unsigned val ) {

    int i;

    addr = ( val << 8 ) | ( addr & 0xFF );
    
    rtc = RTC_RST;
    udelay( 4 );

    for( i = 0; i < 16; i++ ) {
	rtc = RTC_RST | ( addr & 0x01 );
	udelay( 1 );
	rtc = RTC_RST | RTC_SCL | ( addr & 0x01 );
	udelay( 1 );
	addr >>= 1;
    }
    
    rtc = RTC_Z;
}

static int rtc_check( int param, int bcd, int min, int max ) {

    int val;

    if( bcd < 0 )
	return 0;

    if( ( bcd & 0x0F ) > 9 )
	return -1;
    
    val = ( bcd >> 4 ) * 10 + ( bcd & 0x0F );

    if( val < min || val > max )
	return -1;

    rtc_write( 0x80 | ( param << 1 ), bcd );

    return 0;
}

static int rtc_set( int year, int month, int day,
		     int hour, int min, int sec ) {

    if( rtc_check( RTC_YEAR, year, 0, 99 ) ||
	rtc_check( RTC_MONTH, month, 1, 12 ) ||
	rtc_check( RTC_DAY, day, 1, 31 ) ||
	rtc_check( RTC_HOUR, hour, 0, 23 ) ||
	rtc_check( RTC_MIN, min, 0, 59 ) ||
	rtc_check( RTC_SEC, sec, 0, 59 ) )
	return -1;

    return 0;
}

static unsigned afadc_cmd( unsigned cmd ) {
    
    int i;
    unsigned val = 0;
    
    afadc = 0;
//    udelay( 5 );

    for( i = 0; i < 24; i++ ) {
	afadc = ( cmd & 0x800000 ) ? AFADC_SDI : 0;
//	udelay( 5 );
	val = ( val << 1 ) | ( afadc & 1 );
	afadc = AFADC_SCL | ( ( cmd & 0x800000 ) ? AFADC_SDI : 0 );
//	udelay( 5 );
	cmd <<= 1;	
    }

    afadc = AFADC_CS;

//    delay( 3 );
    
    return val;
}

static void lcd_init( void ) {

    lcd = LCD_RESET | LCD_CS;
    delay( 150 );
    
    lcd = LCD_CS;
    delay( 1 );

    lcd = LCD_RESET | LCD_CS;
    delay( 150 );
    
    lcd_cmd( 0x11 ); // SLPOUT
    lcd_cmd( 0x29 ); // DISPON
	
    lcd_cmd( 0x36 ); // MADCTL
    lcd_dat( 0x10 );
	
    lcd_cmd( 0xDF ); // CMD2EN
    lcd_dat( 0x5A );
    lcd_dat( 0x69 );
    lcd_dat( 0x02 );
    lcd_dat( 0x01 );
	
    lcd_cmd( 0xB0 ); // RAMCTL
    lcd_dat( 0x11 ); // RAM access from RGB, display RGA interface
    lcd_dat( 0xC0 ); // RIM 0, 18 bit width
	
    lcd_cmd( 0xB1 ); // RGBCTRL
    lcd_dat( 0xC0 ); // Shift register, DE mode
    lcd_dat( 0x02 );
    lcd_dat( 0x14 );
}

static char ascii( unsigned char mod, unsigned char code ) {

    char unshifted[ 27 ] = {
	'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '\r', '\e',
	'\b', '\t', ' ', '-', '=', '[', ']', '\\', 0, ';', '\'', '`',
	',', '.', '/' },
	shifted[ 27 ] = {
	'!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '\r', '\e',
	'\b', '\t', ' ', '_', '+', '{', '}', '|', 0, ':', '"', '~',
	'<', '>', '?'
	};

    if( code < 30 )
	return code - 4 + ( mod & 0x22 ? 'A' : 'a' ); // alphabetic
    else if( code < 57 )
	return ( mod & 0x22 ? shifted : unshifted )[ code - 30 ];
    else if( code == 57 )
	return 0; // caps lock
    else if( code < 70 )
	return 0x80 + code - 58; // function keys
    else
	return 0;
}

static char kbd_poll( void ) {
    
    static unsigned char prev[ 2 ][ 6 ];
    char c = 0;
    unsigned char buf[ 8 ];
    int i, j, port;    
    static char repeat[ 2 ];

    if( panelkey ) {
	char c = 0;

	switch( panelkey ) {
	case PANEL_L:
	    c = '\e';
	    break;
	    
	case PANEL_R:
	    c = 0x83; // CQ
	    break;

	case PANEL_ROT_L:
	    c = 0x8A; // key
	    break;

	case PANEL_ROT_R:
	    c = 0xFF; // ???
	    break;
	}
	
	panelkey = PANEL_NONE;

	return c;
    }
    
    for( port = 0; port < 2; port++ )
	if( kbd.stat & ( 1 << port ) ) {
	    for( i = 0; i < 8; i++ )
		buf[ i ] = kbd.report[ port ][ i ];

	    kbd.stat = 1 << port;

	    if( buf[ 2 ] > 0 && buf[ 2 ] < 4 )
		/* error */
		return 0;

	    if( prev[ port ][ 0 ] && !prev[ port ][ 1 ] &&
		buf[ 2 ] == prev[ port ][ 0 ] && !buf[ 3 ] ) {
		/* single keypress autorepeat */
		if( repeat[ port ] )
		    return ascii( buf[ 0 ], buf[ 2 ] );
		else
		    repeat[ port ] = 1;
	    } else
		repeat[ port ] = 0;
	
	    for( i = 2; i < 8; i++ )
		if( buf[ i ] >= 4 ) {
		    for( j = 0; j < 6; j++ )
			if( buf[ i ] == prev[ port ][ j ] )
			    break;

		    if( j == 6 )
			c = ascii( buf[ 0 ], buf[ i ] );
		}
	    
	    for( i = 0; i < 6; i++ )
		prev[ port ][ i ] = buf[ i + 2 ];
	}

    if( c )
	return c;

    if( kbd.stat & 0x04 ) {
	static int len;
	static int header;
	static int ignore;
	
	if( len && header == 8 ) {	    
	    len--;

	    if( !len )
		header = 0;
	    
	    return kbd.report[ 2 ][ 0 ];
	} else {
	    unsigned char val = kbd.report[ 2 ][ 0 ];

	    if( !header )
		ignore = val & 0x80;
	    
	    if( header == 6 && !ignore )
		len = val;

	    if( header == 7 && val )
		len = 0;
	    
	    header++;

	    if( header == 8 && !len )
		header = 0;
	}
    }
    
    return 0;
}

static void set_dac_freq( unsigned freq ) {

#if OLD_RX // single conversion receiver, no longer used
    dac.freq = ( (unsigned long long) freq * 184467440736ULL ) >> 32;
    /* IF passband centre 446429 Hz, resampled passband centre 1829 Hz:
       offset 444600 Hz */
    dac.lofreq = ( (unsigned long long) ( mode == MODE_CW ?
					  freq - 444600 - 700 :
					  freq - 444600 ) *
		   184467440736ULL ) >> 32;
#else
    dac.freq = ( (unsigned long long) freq * 184467440736ULL ) >> 32;
    /* IF passband centre 446429 Hz, resampled passband centre 1829 Hz:
       offset 444600 Hz */
    dac.lofreq = ( (unsigned long long) ( mode == MODE_CW ?
					  44444600 - freq - 700 :
					  44444600 - freq ) *
		   184467440736ULL ) >> 32;
    
#endif
}

static int diff8( unsigned char new, unsigned char old ) {

    int udiff = ( new - old ) & 0xFF;
    
    if( udiff < 0x80 )
	return udiff;
    else
	return udiff - 0x100;
}

// static void usb_transfer( unsigned char *buf, int len ) {
// 
//     int i;
//     
//     tty( "USB transfer: type " );
//     tty_hex( buf[ 0 ], 2 );
//     tty( ", len " );
//     tty_hex( len, 2 );
//     ttychar( '\n' );
// 
//     for( i = 8; i < len; i++ ) {
// 	tty_hex( buf[ i ], 2 );
// 	ttychar( ' ' );
//     }
// 
//     ttychar( '\n' );
// }

static int decibels( unsigned power ) {

    int db = 0;
    unsigned lim = 0xFFFFFFFF;

    while( power < lim ) {
	db--;
	lim = ( lim * 3411613790ULL ) >> 32;
    }

    return db;
}

static void idle( void ) {
    
    unsigned newswitch = led;
    int i;
    int tr;
    static unsigned short coarse, fine; // , coarseprev, fineprev;
//    static unsigned tprev;
//    unsigned tnew = rdcycle() & 0xFFFF0000;
    
    if( !panelkey ) {
	if( ( oldswitch & 0x00040000 ) && !( newswitch & 0x00040000 ) )
	    panelkey = PANEL_L;
	else if( ( oldswitch & 0x00080000 ) && !( newswitch & 0x00080000 ) )
	    panelkey = PANEL_R;
	else if( ( oldswitch & 0x00010000 ) && !( newswitch & 0x00010000 ) )
	    panelkey = PANEL_ROT_L;
	else if( ( oldswitch & 0x00020000 ) && !( newswitch & 0x00020000 ) )
	    panelkey = PANEL_ROT_R;
    }

    // FIXME left switch change band instead
#if 0
    freq += diff8( newswitch & 0xFF, oldswitch & 0xFF ) * 10000;
    freq += diff8( ( newswitch >> 8 ) & 0xFF,
		   ( oldswitch >> 8 ) & 0xFF ) * 100;
#else
    // QRP ARCI SHS
    if( diff8( newswitch & 0xFF, oldswitch & 0xFF ) < 0 )
	freq = 7030000;
    else if( diff8( newswitch & 0xFF, oldswitch & 0xFF ) > 0 )
	freq = 14060000;

    freq += diff8( ( newswitch >> 8 ) & 0xFF,
		   ( oldswitch >> 8 ) & 0xFF ) * 100;    
#endif
	
    oldswitch = newswitch;

    {
//	int i;
	static int init;

	if( !init ) {
	    delay( 5 );
	    
	    afadc_cmd( 0x080210 ); // append channel ID
	    afadc_cmd( 0x080307 ); // 128 sample average
	    afadc_cmd( 0x08050A ); // channels 1/3 GPIO
	    afadc_cmd( 0x08070A ); // channels 1/3 output
	    afadc_cmd( 0x08090A ); // channels 1/3 push/pull
	    afadc_cmd( 0x080B0A ); // channels 1/3 high
	}
	
#if 0 // manual seq
	for( i = 0; i < 8; i++ ) {
	    afadc_cmd( 0x081100 | i ); // select channel
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    afadc_cmd( 0x000000 ); // convert
	    delay( 1 );
	    afadc_cmd( 0x000000 ); // convert
	    show_hex( 0, 5 + i, afadc_cmd( 0x000000 ) >> 4, 5, 0x0F );
	}
#else // auto seq
	{
	    unsigned n;

	    if( !init ) {
		afadc_cmd( 0x081011 ); // auto seq start
		afadc_cmd( 0x0812F5 ); // auto seq channels 3-7
	    }
	    
	    delay( 1 );
	    n = afadc_cmd( 0x000000 );	

//	    show_hex( 0, 5 + ( ( n >> 4 ) & 7 ), n >> 4, 5, 0x0F );

//	    if( ( ( n >> 4 ) & 7 ) == 5 ) {
//		for( i = 0; i < 40; i++ )
//		    screen[ 13 ][ i ] = 0x0F00 | ( ( ( n >> 16 ) & 0x1F ) == i ? '*' : ' ' );
//	    }

	    if( ( ( n >> 4 ) & 7 ) == 6 ) {
		coarse = n >> 8;
		if( coarse < 0x0020 )
		    coarse = 0x0020;
		else if( coarse > 0xFFE0 )
		    coarse = 0xFFE0;
	    } else if( ( ( n >> 4 ) & 7 ) == 7 ) {
		fine = n >> 8;
		if( fine < 0x0020 )
		    fine = 0x0020;
		else if( fine > 0xFFE0 )
		    fine = 0xFFE0;
	    }
	}
#endif

	init = 1;
    }

#if 0 // pot ADC tuning
    if( tnew != tprev ) {
	unsigned round = ( freq + 500 ) / 1000 * 1000;
	unsigned bot, top;
	struct segment *s;
	static int coarseunlock, fineunlock;
	
	for( s = segments; s->low <= freq; s++ )
	    ;

	if( s > segments )
	    s--;
	
	while( s > segments && s->low == ( s - 1 )->high )
	    s--;

	bot = s->low;

	while( s < segments + ( sizeof segments ) /
	       ( sizeof *segments ) - 1 && s->high == ( s + 1 )->low )
	    s++;
	
	top = s->high;

	if( round - freq == 500 && fine < 0x8000 )
	    round -= 1000;

	if( !coarseprev )
	    coarseprev = coarse;
	
	if( !fineprev )
	    fineprev = fine;
	
	if( coarse < coarseprev - ( coarseunlock ? 1 : 4 ) ||
	    coarse > coarseprev + ( coarseunlock ? 1 : 4 ) ) {
	    coarseunlock = 0x80;
	    freq = bot - 20000 + ( ( ( ( top + 40000 - bot ) >> 8 ) *
				     (unsigned short) ~coarse ) >> 8 );
	    
	    freq -= freq % 1000;
	    round = freq;
	}
	
	if( fine < fineprev - ( fineunlock ? 1 : 4 ) ||
	    fine > fineprev + ( fineunlock ? 1 : 4 ) ) {
	    int step = (unsigned short) ~fine / 6554;
	    int off = (unsigned short) ~fine % 6554;

	    fineunlock = 0x80;
	    
	    if( off < 1277 )
		off = 0;
	    else if( off < 5277 )
		off = ( off - 1277 ) / 40;
	    else {
		step++;
		off = 0;
	    }

	    freq = round - 500 + step * 100 + off;
	}

	if( freq < bot )
	    freq = bot;
	else if( freq > top )
	    freq = top;

	tprev = tnew;
	coarseprev = coarse;
	fineprev = fine;

	if( coarseunlock )
	    coarseunlock--;

	if( fineunlock )
	    fineunlock--;
    }
#endif
    
    if( freq < 30000 )
	freq = 30000;

    if( freq > 30000000 )
	freq = 30000000;

    tr = dac.stat & DAC_STAT_TX;
    
    if( freq != oldfreq || tr != oldtr ) {
	struct segment *s;
	    
	set_dac_freq( freq );
	show_dec_big( 0, 1, freq / 1000000, 2, 0, tr ? 0x09 : 0x0F );
	show_dec_big( 5, 1, ( freq / 1000 ) % 1000, 3, 1, tr ? 0x09 : 0x0F );
	show_dec_big( 12, 1, freq % 1000, 3, 1, tr ? 0x09 : 0x0F );
	screen[ 2 ][ 4 ] = tr ? 0x091E : 0x0F1E;

	for( i = 0; i < 0x10; i++ )
	    screen[ 3 ][ 8 + i ] = 0;

	for( s = segments; s < segments + ( sizeof segments ) /
		 ( sizeof *segments ); s++ )
	    if( freq >= s->low && freq <= s->high ) {
		display( 8, 3, s->label, 0x07 );
		break;
	    }

	oldfreq = freq;
	oldtr = tr;
    }

    switch( mode ) {
    case MODE_CW:
	display( 23, 2, " CW ", 0x4B );
	display( 28, 2, " Iambic ", 0xB0 ); // FIXME keyer
	display( 37, 2, "13", 0x0B ); // FIXME speed

	break;

    case MODE_WSPR:
	display( 23, 2, " WSPR ", 0x4B );
	for( i = 29; i < 40; i++ )
	    screen[ 2 ][ i ] = 0;

	break;
	
    case MODE_FT8:
	display( 23, 2, " FT8 ", 0x4B );
	// FIXME odd/even
	for( i = 28; i < 40; i++ )
	    screen[ 2 ][ i ] = 0;

	break;
	

    case MODE_LSB:
	display( 23, 2, " LSB ", 0x4B );
	for( i = 28; i < 40; i++ )
	    screen[ 2 ][ i ] = 0;

	break;
	

    case MODE_USB:
	display( 23, 2, " USB ", 0x4B );
	for( i = 28; i < 40; i++ )
	    screen[ 2 ][ i ] = 0;

	break;	
    }
    
    show_hex( 38, 0, rtc_read( 0x81 | ( RTC_SEC << 1 ) ), 2, 0x0A );
    show_hex( 35, 0, rtc_read( 0x81 | ( RTC_MIN << 1 ) ), 2, 0x0A );
    show_hex( 32, 0, rtc_read( 0x81 | ( RTC_HOUR << 1 ) ), 2, 0x0A );
	
    show_hex( 29, 0, rtc_read( 0x81 | ( RTC_DAY << 1 ) ), 2, 0x0A );
    show_hex( 26, 0, rtc_read( 0x81 | ( RTC_MONTH << 1 ) ), 2, 0x0A );
    show_hex( 23, 0, rtc_read( 0x81 | ( RTC_YEAR << 1 ) ), 2, 0x0A );

#if 1
    {
	show_dec( 26, 3, afgain, 1, 0x0A );
	show_signed_dec( 28, 3, decibels( rfadc.rfpwr ) - 20, 4, 0x0A );
	display( 33, 3, "dBm", 0x0A );
    
	show_dec( 26, 4, rfgain, 1, 0x0A );
	show_signed_dec( 28, 4, decibels( rfadc.chanpwr ) - 20 - rfgain * 6,
			 4, 0x0A );
	display( 33, 4, "dBm", 0x0A );

	if( txatten ) {
	    screen[ 4 ][ 21 ] = '-' | 0x0900;
	    show_dec( 22, 4, txatten, 1, 0x09 );
	} else
	    screen[ 4 ][ 21 ] = screen[ 4 ][ 22 ] = 0;
	
	show_signed_dec( 36, 4, temperature[ ( rtc >> 16 ) & 0x3F ], 3, 0x0A );
	screen[ 4 ][ 39 ] = 0x0A1E;
    }
#endif
}

#define CODE(x)							\
    ( ( ( (x) & 030000000 ) >> 7 ) |				\
      ( ( (x) & 003000000 ) >> 6 ) |				\
      ( ( (x) & 000300000 ) >> 5 ) |				\
      ( ( (x) & 000030000 ) >> 4 ) |				\
      ( ( (x) & 000003000 ) >> 3 ) |				\
      ( ( (x) & 000000300 ) >> 2 ) |				\
      ( ( (x) & 000000030 ) >> 1 ) |				\
      ( ( (x) & 000000003 ) >> 0 ) )

static const unsigned short code[ 64 ] = {
    CODE( 030000000 ), // space
    CODE( 021212200 ), // !
    CODE( 012112100 ), // "
    CODE( 030000000 ), // # -- treat as space
    CODE( 011121120 ), // $
    CODE( 030000000 ), // % -- treat as space
    CODE( 012111000 ), // &
    CODE( 012222100 ), // '
    CODE( 021221000 ), // (
    CODE( 021221200 ), // )
    CODE( 030000000 ), // * -- treat as space
    CODE( 012121000 ), // +
    CODE( 022112200 ), // ,
    CODE( 021111200 ), // -
    CODE( 012121200 ), // .
    CODE( 021121000 ), // /
    CODE( 022222000 ), // 0
    CODE( 012222000 ), // 1
    CODE( 011222000 ), // 2
    CODE( 011122000 ), // 3
    CODE( 011112000 ), // 4
    CODE( 011111000 ), // 5
    CODE( 021111000 ), // 6
    CODE( 022111000 ), // 7
    CODE( 022211000 ), // 8
    CODE( 022221000 ), // 9
    CODE( 022211100 ), // :
    CODE( 021212100 ), // ;
    CODE( 021221000 ), // < -- treat as (
    CODE( 021112000 ), // =
    CODE( 021221200 ), // > -- treat as )
    CODE( 011221100 ), // ?
    CODE( 012212100 ), // @
    CODE( 012000000 ), // A
    CODE( 021110000 ), // B
    CODE( 021210000 ), // C
    CODE( 021100000 ), // D
    CODE( 010000000 ), // E
    CODE( 011210000 ), // F
    CODE( 022100000 ), // G
    CODE( 011110000 ), // H
    CODE( 011000000 ), // I
    CODE( 012220000 ), // J
    CODE( 021200000 ), // K
    CODE( 012110000 ), // L
    CODE( 022000000 ), // M
    CODE( 021000000 ), // N
    CODE( 022200000 ), // O
    CODE( 012210000 ), // P
    CODE( 022120000 ), // Q
    CODE( 012100000 ), // R
    CODE( 011100000 ), // S
    CODE( 020000000 ), // T
    CODE( 011200000 ), // U
    CODE( 011120000 ), // V
    CODE( 012200000 ), // W
    CODE( 021120000 ), // X
    CODE( 021220000 ), // Y
    CODE( 022110000 ), // Z
    CODE( 021221000 ), // [ -- treat as (
    CODE( 021121000 ), // \ -- treat as /
    CODE( 021221200 ), // ] -- treat as )
    CODE( 030000000 ), // ^ -- treat as space
    CODE( 011221200 )  // _
};

static int key_press( void ) {

    char c = kbd_poll();

    if( c == '\e' )
	return -1;

    // FIXME buffer other characters?
    
    return 0;
}

static int wait_up( void ) {
    
    unsigned int s;
    
    while( ( s = dac.stat ) & DAC_STAT_KEY_DOWN ) {
	if( !( s & DAC_STAT_AUTO_ACTIVE ) || key_press() < 0 )
	    return -1;

	idle();
    }

    return 0;
}

static int wait_down( void ) {
    
    unsigned int s;

    while( !( ( s = dac.stat ) & DAC_STAT_KEY_DOWN ) ) {
	if( !( s & DAC_STAT_AUTO_ACTIVE ) || key_press() < 0 )
	    return -1;
	idle();
    }

    return 0;
}

static struct command {
    char *name;
    char *desc;
    void ( *func )( char * );
} commands[];

static void cmd_afgain( char *p ) {

    int gain = parse_dec( p );

    if( gain < 0 ) {
	tty( "Invalid number.\n" );
	return;
    }

    if( gain > 17 ) {
	tty( "AF gain must be at most 17.\n" );
	return;
    }

    rfadc.afgain = afgain = gain;

    tty( "AF gain set.\n" );
}

static void cmd_atten( char *p ) {

    int loss = parse_dec( p );

    if( loss < 0 ) {
	tty( "Invalid number.\n" );
	return;
    }

    if( loss > 9 ) {
	tty( "AF gain must be at most 9.\n" );
	return;
    }

    txatten = loss;
    dac.stat = ( waitunit << 16 ) | ( txatten << 24 );

    tty( "TX attenuation set.\n" );
}

static void cmd_bias( char *p ) {

    int bias = parse_dec( p );

    if( bias < 0 ) {
	tty( "Invalid number.\n" );
	return;
    }

    if( bias > 0xFF ) {
	tty( "Bias must be at most 255.\n" );
	return;
    }

    rfadc.rxbias = bias;

    tty( "RX amplifier bias set.\n" );
}

static void cmd_chirp( char *p ) {

    int i, delta;
    int base = ( (unsigned long long) freq * 184467440737ULL ) >> 32;
    
    dac.stat = DAC_KEYER_REQ | DAC_KEYER_HOLD | ( waitunit << 16 ) |
	( txatten << 24 );

    for( i = 0; i < 3; i++ )
	for( delta = 0; delta < 400; delta++ ) {
	    unsigned done;
	    
	    dac.freq = base + ( delta << 6 );

	    done = rdcycle() + 25000;

	    while( ( done - rdcycle() ) <= 25000 )
		;
	}
    
    dac.stat = ( waitunit << 16 ) | ( txatten << 24 );
    dac.freq = base;
}

static void cmd_cw( char *p ) {

    mode = MODE_CW;
    set_dac_freq( freq );
}

static void cmd_ft8( char *p ) {

    mode = MODE_FT8;
    set_dac_freq( freq );

    // FIXME allow message transmission
}

static void cmd_help( char *p ) {

    int i;

    // FIXME pager
    
    for( i = 0; i < NUM_COMMANDS; i++ ) {
	tty( commands[ i ].name );
	ttychar( ':' );
	ttychar( ' ' );
	tty( commands[ i ].desc );
	ttychar( '\n' );
    }
}

static void cmd_key( char *p ) {

    int started = 0;
    int inchar;
    int x;

    for( x = 0; x < 40; x++ )
	screen[ 14 ][ x ] = 0xA000 | ' ';

    x = 0;

    if( !*p ) {
	dac.stat = DAC_KEY_TX | ( waitunit << 16 ) | ( txatten << 24 );

	while( kbd_poll() != '\e' )
	    // FIXME key other characters
	    idle();

	dac.stat = ( waitunit << 16 ) | ( txatten << 24 );
	
	return;
    }
    
    while( *p ) {
	int i = ( *p & 0x60 ) == 0x20 ? *p & 0x1F : ( *p & 0x1F ) | 0x20;
	unsigned short syms = code[ i ];

	inchar = 0;
	    
	while( 1 ) {
	    switch( syms & 0xC000 ) {
	    case 0x0000: /* end of character */
		if( started && wait_down() )
		    goto stop;

		dac.stat = DAC_KEYER_REQ | DAC_KEYER_UP_CHARACTER |
		    ( waitunit << 16 ) | ( txatten << 24 );

		goto chardone;

	    case 0x4000: /* dit */
		if( inchar && wait_down() )
		    goto stop;
		
		if( started && wait_up() )
		    goto stop;

		dac.stat = DAC_KEYER_REQ | DAC_KEYER_DIT |
		    ( waitunit << 16 ) | ( txatten << 24 );

		break;

	    case 0x8000: /* dah */
		if( inchar && wait_down() )
		    goto stop;
		
		if( started && wait_up() )
		    goto stop;

		dac.stat = DAC_KEYER_REQ | DAC_KEYER_DAH |
		    ( waitunit << 16 ) | ( txatten << 24 );

		break;
		
	    case 0xC000: /* long gap */
		if( started && wait_down() )
		    goto stop;

		dac.stat = DAC_KEYER_REQ | DAC_KEYER_UP_WORD |
		    ( waitunit << 16 ) | ( txatten << 24 );

		goto chardone;
	    }
	
	    syms <<= 2;
	    started = 1;
	    inchar = 1;
	}
	
    chardone:
	screen[ 14 ][ x++ ] = 0xA000 | *p++;
    }

    wait_up();

stop:
    dac.stat = ( waitunit << 16 ) | ( txatten << 24 ); // FIXME set proper mode
}

static void cmd_keydown( char *p ) {

    dac.stat = DAC_KEYER_REQ | DAC_KEYER_HOLD | ( waitunit << 16 ) |
	( txatten << 24 );

    while( !kbd_poll() )
	;
    
    dac.stat = ( waitunit << 16 ) | ( txatten << 24 );
}

static void cmd_rfgain( char *p ) {

    int gain = parse_dec( p );

    if( gain < 0 ) {
	tty( "Invalid number.\n" );
	return;
    }

    if( gain > 15 ) {
	tty( "RF gain must be at most 15.\n" );
	return;
    }

    rfadc.rfgain = rfgain = gain;

    tty( "RF gain set.\n" );
}

static void cmd_speed( char *p ) {

    int speed = parse_dec( p );

    if( speed < 0 ) {
	tty( "Invalid number.\n" );
	return;
    }

    if( speed < 5 ) {
	tty( "Speed must be at least 5 wpm.\n" );
	return;
    }

    if( speed > 50 ) {
	tty( "Speed must be at most 50 wpm.\n" );
	return;
    }

    show_dec( 37, 2, speed, 2, 0x0B );
    
    waitunit = ( 37000000 / speed + 0x4000 ) >> 15;
}

static void cmd_time( char *p ) {

    char *cp;
    int digits = 0;
    char val[ 14 ];
    int ret;
    
    for( cp = p; *cp; cp++ )
	if( *cp >= '0' && *cp <= '9' ) {
	    if( digits < 14 )
		val[ digits ] = *cp - '0';
	    
	    digits++;
	}

    switch( digits ) {
    case 4: // hhmm
	ret = rtc_set( -1, -1, -1, ( val[ 0 ] << 4 ) | val[ 1 ],
		       ( val[ 2 ] << 4 ) | val[ 3 ], 0 );
	break;
	
    case 6: // hhmmss
	ret = rtc_set( -1, -1, -1, ( val[ 0 ] << 4 ) | val[ 1 ],
		       ( val[ 2 ] << 4 ) | val[ 3 ],
		       ( val[ 4 ] << 4 ) | val[ 5 ] );
	break;
	
    case 8: // MMDD hhmm
	ret = rtc_set( -1, ( val[ 0 ] << 4 ) | val[ 1 ],
		       ( val[ 2 ] << 4 ) | val[ 3 ],
		       ( val[ 4 ] << 4 ) | val[ 5 ],
		       ( val[ 6 ] << 4 ) | val[ 7 ], 0 );
	break;
	
    case 12: //  YYMMDD hhmmss
	ret = rtc_set( ( val[ 0 ] << 4 ) | val[ 1 ],
		       ( val[ 2 ] << 4 ) | val[ 3 ],
		       ( val[ 4 ] << 4 ) | val[ 5 ],
		       ( val[ 6 ] << 4 ) | val[ 7 ],
		       ( val[ 8 ] << 4 ) | val[ 9 ],
		       ( val[ 10 ] << 4 ) | val[ 11 ] );
	break;
	
    case 14 : // 20YYMMDD hhmmss
	if( val[ 0 ] == 2 && val[ 1 ] == 0 )
	    ret = rtc_set( ( val[ 2 ] << 4 ) | val[ 3 ],
			   ( val[ 4 ] << 4 ) | val[ 5 ],
			   ( val[ 6 ] << 4 ) | val[ 7 ],
			   ( val[ 8 ] << 4 ) | val[ 9 ],
			   ( val[ 10 ] << 4 ) | val[ 11 ],
			   ( val[ 12 ] << 4 ) | val[ 13 ] );
	else
	    ret = -1;
	
	break;

    default:
	ret = -1;
    }

    tty( ret < 0 ? "Invalid time.\n" : "Time set.\n" );
}

static void cmd_touch( char *p ) {

    int x;
    
    for( x = 0; x < 40; x++ )
	screen[ 14 ][ x ] = 0xA000 | ' ';

    display( 1, 14, "Dit:", 0xA0 );
    display( 17, 14, "Dah:", 0xA0 );
    
    while( !kbd_poll() ) {
	unsigned n = dac.touch;
    
	show_dec( 6, 14, n & 0xFFFF, 4, 0xA0 );
	show_dec( 22, 14, n >> 16, 4, 0xA0 );

	idle();
    }
}

static void cmd_tune( char *p ) {

    int n = parse_dec( p );

    if( n < 0 ) {
	tty( "Invalid number.\n" );
	return;
    }
    
    if( n < 30000 ) {
	tty( "Frequency must be at least 30 kHz.\n" );
	return;
    }

    if( n > 30000000 ) {
	tty( "Frequency must be at most 30 MHz.\n" );
	return;
    }

    freq = n;
}

static void cmd_usb( char *p ) {

    mode = MODE_USB;
    set_dac_freq( freq );
}

static void cmd_wspr( char *p ) {

    static char msg[ 162 ] = {
	1, 1, 0, 0, 0, 2, 0, 2, 3, 0, 2, 0, 1, 1, 3, 0, 0, 0, 3, 2, 2, 3, 2, 1,
	3, 1, 3, 0, 0, 2, 2, 0, 2, 0, 1, 0, 0, 3, 0, 3, 0, 2, 2, 0, 2, 0, 3, 2,
	1, 3, 2, 2, 1, 3, 0, 3, 0, 2, 0, 3, 3, 2, 1, 0, 2, 0, 0, 3, 3, 0, 3, 2,
	3, 2, 1, 2, 3, 2, 2, 3, 0, 0, 1, 0, 1, 3, 2, 2, 0, 3, 1, 2, 1, 2, 3, 2,
	2, 2, 3, 0, 2, 2, 0, 2, 3, 2, 2, 3, 0, 0, 3, 1, 1, 0, 1, 1, 0, 2, 1, 1,
	0, 3, 0, 2, 2, 1, 1, 1, 0, 2, 0, 0, 0, 1, 0, 3, 0, 2, 3, 1, 0, 2, 2, 2,
	2, 0, 0, 3, 3, 0, 1, 2, 1, 1, 2, 2, 0, 1, 1, 2, 0, 0
    };
    int i;
    int sym[ 4 ];
    unsigned t1;

    mode = MODE_WSPR;
    idle();
    
    for( i = 0; i < 4; i++ )
	sym[ i ] = ( (unsigned long long) freq * 184467440737ULL +
		     (unsigned long long) i * 270215977642ULL ) >> 32;
    
    dac.stat = DAC_KEYER_REQ | DAC_KEYER_HOLD | ( waitunit << 16 ) |
	( txatten << 24 );

    t1 = rdcycle() + 17066667;
    
    for( i = 0; i < 162; i++ ) {
	dac.freq = sym[ (int) msg[ i ] ];
	
	idle(); // FIXME disable tuning
	
	if( key_press() < 0 )
	    break;

	while( ( t1 - rdcycle() ) < 0x2000000 )
	    ;
	
	t1 += 17066667;
    }
    
    dac.stat = ( waitunit << 16 ) | ( txatten << 24 );
    dac.freq = sym[ 0 ];
}

static void read_adc( char *p ) {

    int n = afadc_cmd( 0x000080 );
    show_hex( 0, 0, n, 8, 0x0F );
}

static void digi_tx( unsigned txfreq, unsigned char *encmsg ) {

    char msg[ 79 ];
    int sym[ 8 ];
    int i;
    unsigned t1;
    unsigned oldfreq = freq;
    
    // FIXME check mode
    
    freq = txfreq;
    idle(); // FIXME disable tuning
    
    for( i = 0; i < 8; i++ )
	sym[ i ] = ( (unsigned long long) txfreq * 184467440737ULL +
		     (unsigned long long) i * 1152921504605ULL +
		     576460752302ULL ) >> 32;

    msg[ 0 ] = msg[ 36 ] = msg[ 72 ] = 3;
    msg[ 1 ] = msg[ 37 ] = msg[ 73 ] = 1;
    msg[ 2 ] = msg[ 38 ] = msg[ 74 ] = 4;
    msg[ 3 ] = msg[ 39 ] = msg[ 75 ] = 0;
    msg[ 4 ] = msg[ 40 ] = msg[ 76 ] = 6;
    msg[ 5 ] = msg[ 41 ] = msg[ 77 ] = 5;
    msg[ 6 ] = msg[ 42 ] = msg[ 78 ] = 2;

    for( i = 0; i < 14; i++ ) {
	msg[ 7 + ( i << 1 ) + 0 ] = encmsg[ i ] & 7;
	msg[ 7 + ( i << 1 ) + 1 ] = ( encmsg[ i ] >> 3 ) & 7;
    }

    msg[ 35 ] = encmsg[ 14 ] & 7;
    msg[ 43 ] = ( encmsg[ 14 ] >> 3 ) & 7;

    for( i = 15; i < 29; i++ ) {
	msg[ 14 + ( i << 1 ) + 0 ] = encmsg[ i ] & 7;
	msg[ 14 + ( i << 1 ) + 1 ] = ( encmsg[ i ] >> 3 ) & 7;
    }
	
    dac.stat = DAC_KEYER_REQ | DAC_KEYER_HOLD | ( waitunit << 16 ) |
	( txatten << 24 );

    t1 = rdcycle() + 4000000;
    
    for( i = 0; i < 79; i++ ) {
	dac.freq = sym[ (int) msg[ i ] ];

	idle(); // FIXME disable tuning

	if( key_press() < 0 )
	    break;
	
	while( ( t1 - rdcycle() ) < 0x2000000 )
	    ;
	
	t1 += 4000000;
    }
    
    dac.stat = ( waitunit << 16 ) | ( txatten << 24 );
    set_dac_freq( freq = oldfreq );
}

static void not_implemented( char *p ) {

    int x = 0;
    unsigned els = 0;
    int shift = 14;
    
    while( 1 ) {
	int old, new;
	
	while( !( dac.stat & DAC_STAT_KEY_DOWN ) )
	    if( kbd_poll() )
		return;
	    else
		idle();

	if( dac.stat & DAC_STAT_MANUAL_DAH )
	    els |= 0x02 << shift;
	else
	    els |= 0x01 << shift;

	if( shift )
	    shift -= 2;

	while( dac.stat & DAC_STAT_KEY_DOWN )
	    if( kbd_poll() )
		return;
	    else
		idle();
	
	old = 0;

	while( !( dac.stat & DAC_STAT_KEY_DOWN ) ) {
	    new = dac.stat & 3;

	    if( new > old ) {
		old = new;

		if( new == 1 ) {
		    int i;
		    
		    for( i = 0; i < 64; i++ )
			if( code[ i ] == els ) {
			    screen[ 13 ][ x++ ] = 0x0A00 | ( i + 32 );
			    
			    break;
			}
		    
		    els = 0;
		    shift = 14;
		} else if( new == 2 )
		    screen[ 13 ][ x++ ] = 0x0A00 | ' ';
	    }

	    screen[ 13 ][ x ] = 0xA000 | ' ';
	    
	    if( kbd_poll() )
		return;
	    else
		idle();
	}
    }

    return;





    
    tty( "Command not implemented.\n" );
}

static struct command commands[] = {
    { "?", "show help for commands", cmd_help },
    { "afgain", "set audio amplification", cmd_afgain },
    { "atten", "set TX attenuation", cmd_atten },
    { "bias", "set RX amplifier bias", cmd_bias },
    { "chirp", "transmit a sweeping frequency", cmd_chirp },
    { "cw", "change to continuous wave mode", cmd_cw },
    { "filter", "set filter bandwidth", not_implemented },
    { "ft8", "change to FT8 mode", cmd_ft8 },
    { "help", "show help for commands", cmd_help },
    { "iambic", "change to iambic keying mode", not_implemented },
    { "key", "transmit a message", cmd_key },
    { "keydown", "transmit continuously", cmd_keydown },
    { "lsb", "change to lower sideband mode", not_implemented },
    { "r", "read ADC", read_adc },
    { "rfgain", "set radio amplification", cmd_rfgain },
    { "speed", "set CW keyer speed", cmd_speed },
    { "straight", "change to straight keying mode", not_implemented },
    { "time", "set the date and time", cmd_time },
    { "touch", "show touch keyer response", cmd_touch },
    { "tune", "change frequency", cmd_tune },
    { "usb", "change to upper sideband mode", cmd_usb },
    { "wspr", "transmit a WSPR report", cmd_wspr }
};

static struct command *match_command( char *p, int *len ) {

    struct command *cp0, *cp;
    struct command *done = commands + NUM_COMMANDS;
    char *p0, *p1;

    (void) sizeof (char[ 1 - 2 * ( ( sizeof commands ) /
				   ( sizeof *commands ) != NUM_COMMANDS ) ]);
    
    for( cp0 = commands; cp0 < done; cp0++ ) {
	for( p0 = cp0->name, p1 = p; *p0 && *p1; p0++, p1++ )
	    if( *p0 != *p1 )
		goto next_command;

	if( *p1 && *p1 != ' ' )
	    continue;
	
	if( !*p0 ) {
	    *len = p1 - p;
	    return cp0;
	}

	break;
	
    next_command:
	;
    }

    if( cp0 >= done )
	return NULL;

    while( *p0 )
	p0++;

    *len = p0 - cp0->name;
    
    for( cp = cp0 + 1; cp < done; cp++ ) {
	for( p0 = cp->name, p1 = p; *p0 && *p1; p0++, p1++ )
	    if( *p0 != *p1 )
		return cp0;

	for( p1 = cp0->name + ( p0 - cp->name ); *p0 == *p1; p0++, p1++ )
	    ;

	if( p0 - cp->name < *len )
	    *len = p0 - cp->name;
    }

    return cp0;
}

extern void main( void ) {

    int i;
    
    set_dac_freq( freq );

    lcd_init();
    
    rtc_write( 0x80 | ( RTC_CHARGE << 1 ), RTC_CHARGE_VAL );

    // FIXME restore state from RTC RAM

    afadc_cmd( 0x080101 ); // reset

    rfadc.rfgain = rfgain;
    rfadc.afgain = afgain;
    rfadc.rxbias = 0xFF;
    
    oldswitch = led;

    // FIXME move to block RAM init
    screen[ 0 ][ 21 ] = 0x0A00 | '2';
    screen[ 0 ][ 22 ] = 0x0A00 | '0';
    screen[ 0 ][ 25 ] = 0x0A00 | '-';
    screen[ 0 ][ 28 ] = 0x0A00 | '-';
    screen[ 0 ][ 34 ] = 0x0A00 | ':';
    screen[ 0 ][ 37 ] = 0x0A00 | ':';
    display( 19, 2, "MHz", 0x0F );

    for( i = 0; i < 40; i++ )
	screen[ 5 ][ i ] = 0x0F11;

    while( 1 ) {
	char buf[ 38 ];
	int len = 0;

	dac.stat = ( waitunit << 16 ) | ( txatten << 24 );
	
	screen[ 14 ][ 0 ] = 0x4F00 | '>';
	for( i = 1; i < 40; i++ )
	    screen[ 14 ][ i ] = 0x4F00 | ' ';
	
	while( 1 ) {
	    char c;
	    char *p;
	    struct command *cp;
	    int match_len;
	    static int digifreq;
	    static int digistate;
	    static unsigned char digimsg[ 0x20 ];
	    
	    screen[ 14 ][ len + 2 ] = 0xF400 | ' ';
	    
	    idle();

	    switch( ( c = kbd_poll() ) ) {
	    case 0:
		continue;

	    case 0x80:
	    case 0x81:
	    case 0x82:
	    case 0x83:
	    case 0x84:
	    case 0x85:
	    case 0x86:
	    case 0x87:
	    case 0x88:
	    case 0x89:
	    case 0x8A:
	    case 0x8B:
		if( bindings[ c - 0x80 ] ) {
		    cmd_key( bindings[ c - 0x80 ] );
		
		    screen[ 14 ][ 0 ] = 0x4F00 | '>';
		    screen[ 14 ][ 1 ] = 0x4F00 | ' ';
		    for( i = 0; i < len; i++ )
			screen[ 14 ][ i + 2 ] = 0x4F00 | buf[ i ];
		    while( i < 38 )
			screen[ 14 ][ i++ + 2 ] = 0x4F00 | ' ';
		}
		continue;
		
	    case '\b':
		if( len ) {
		    screen[ 14 ][ len + 2 ] = 0x4F00 | ' ';
		    len--;
		}
		continue;
		
	    case '\e':
		break;

	    case '\r':
		if( !len )
		    continue;
		
		buf[ len ] = 0;

		for( p = buf; *p; p++ )
		    ttychar( 0x80 | *p );

		ttychar( '\n' );

		if( ( cp = match_command( buf, &match_len ) ) ) {
		    p = buf + match_len;

		    while( *p == ' ' )
			p++;
		    
		    // FIXME clear bottom line???
		    cp->func( p );
		} else
		    tty( "Invalid command.\n" );

		break;

	    case '\t':
		buf[ len ] = 0;

		if( ( cp = match_command( buf, &match_len ) ) ) {
		    while( len < match_len ) {
			screen[ 14 ][ len + 2 ] = 0x4F00 |
			    ( buf[ len ] = cp->name[ len ] );
			len++;
		    }

		    if( !cp->name[ len ] ) { // FIXME not if cmd is prefix of another
			screen[ 14 ][ len + 2 ] = 0x4F00 |
			    ( buf[ len ] = ' ' );
			len++;
		    }
		} else
		    // FIXME beep
		    ;

		continue;

	    case 0xBE:
		digi_tx( digifreq, digimsg );
		continue;
		
	    case 0xBF:
		digifreq = 0;
		digistate = 0;
		continue;
		
	    default:
		if( ( c & 0xC0 ) == 0xC0 ) {
		    if( digistate < 6 )
			digifreq = ( digifreq << 6 ) | ( c & 0x3F );
		    else
			digimsg[ digistate - 6 ] = c & 0x3F;

		    if( digistate < 37 )
			digistate++;
		} else if( c >= ' ' && c <= '~' && len <= 36 ) {
		    buf[ len++ ] = c;
		    screen[ 14 ][ len + 1 ] = 0x4F00 | c;
		}

		continue;
	    }

	    break;
	}
    }
}
