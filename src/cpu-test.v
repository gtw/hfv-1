//
// cpu-test.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.
	
module cputest();
    reg clk = 1'b0;
    always #1 clk = ~clk;

    reg rst;
    wire[ 31:2 ] addr;
    wire[ 31:0 ] wdata;
    wire[ 3:0 ] wsel;
    wire we;
    cpu dut( clk, rst, addr, wdata, wsel, we, 32'b0, 1'b1 );
    
    initial begin
	$dumpfile( "cpu.vcd" );
	$dumpvars( 0, dut );

	rst = 1'b1;
	#10 rst = 1'b0;
    end
endmodule
