//
// asq-test.v
//
// Copyright 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.
	
`ifndef VERBOSE
    `define VERBOSE 0
`endif

module asqtest();
    reg clk = 1'b0;
    always #1 clk = ~clk;

    reg[ 9:0 ] x;
    reg xrdy;
    wire[ 19:0 ] sq;
    wire sqrdy;
    
    asq #( 10 ) dut( clk, x, xrdy, sq, sqrdy );

    integer i, low, high;
    initial begin
	$dumpfile( "asq.vcd" );
	$dumpvars( 0, dut );
	x = 0;
	xrdy = 0;
	for( i = 0; i < 'h400; i++ ) begin
	    low = ( ( i * i ) >> 1 ) + ( ( i * i ) >> 2 );
	    high = i * i;
	    x = i;
	    #2;
	    xrdy = 1;
	    #4 xrdy = 0;
	    #4;
	    while( !sqrdy )
		#1;
	    if( `VERBOSE )
		$display( "%5d %8d %8d %8d", i, low, sq, high );
	    
	    if( sq > high )
		$fatal( 2, "asq of %5d is %8d, greater than %8d", 
			i, sq, high );
	    if( sq < low )
		$fatal( 2, "asq of %5d is %8d, less than %8d", 
			i, sq, low );
	end
	$info( "Success." );
	$finish();
    end

    initial
	#32768 $fatal( 2, "timeout" );
endmodule
