//
// cpu.v
//
// Copyright 2020, 2021, 2022, 2023 Gary Wong <gtw@gnu.org>
//

// This file is part of HFV-1.
// 
// HFV-1 is free software and a free hardware design: you can
// redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.
// 
// HFV-1 is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with HFV-1. If not, see <https://www.gnu.org/licenses/>.

// simple (non-pipelined) RISC-V CPU
module cpu( input clk, input rst, output[ 31:2 ] addr,
	    output[ 31:0 ] wdata, output[ 3:0 ] wsel, output we,
	    input[ 31:0 ] rdata, input ack );
    
    // Architectural state
    reg[ 31:0 ] pc = 32'h00000000; // FIXME need only 31:2
    
    reg[ 31:0 ] regs[ 1:31 ];
    
    reg[ 31:0 ] mem[ 0:16383 ];
`ifdef SYNTHESIS
    initial $readmemh( "random.hex", mem );
`else
    string filename;
    
    initial begin
	if( $value$plusargs( "load=%s", filename ) ) begin
	    $display( "Loading memory from %s", filename );
	    $readmemh( filename, mem );
	end else begin
	    $display( "Loading default memory" );
	    mem[ 0 ] = 32'h07300513; // li a0, 0x73
	    mem[ 1 ] = 32'h00000073; // ecall
	    mem[ 2 ] = 0;
	end
	
	#32768 $fatal( 2, "timeout" );
    end
`endif
    
    // Internal state
    enum {
	CYCLE_NORMAL, CYCLE_READ, CYCLE_WRITE
    } cycle_type, new_cycle_type;
    initial
	cycle_type = CYCLE_READ; // dummy read before instruction fetch

    always_ff @( posedge clk )
	cycle_type <= new_cycle_type;
    
    reg[ 31:0 ] instruction;
    reg[ 31:0 ] old_instruction = 32'h00000013;

    reg[ 31:0 ] cycles = 32'h00000000;
    
    always_comb
`ifndef SYNTHESIS
	/* priority */ if( rst )
	    instruction = 32'h00000013; // NOP
	else
`endif
	/* unique */ if( cycle_type == CYCLE_NORMAL )
	    instruction = memread;
	else
	    instruction = old_instruction;
    
    always_ff @( posedge clk )
	old_instruction <= instruction;
	
    typedef enum bit[ 4:0 ] {
	OP_LOAD     = 5'b00000,
	OP_MISC_MEM = 5'b00011,
	OP_OP_IMM   = 5'b00100,
	OP_AUIPC    = 5'b00101,
	OP_STORE    = 5'b01000,
	OP_OP       = 5'b01100,
	OP_LUI      = 5'b01101,
	OP_BRANCH   = 5'b11000,
	OP_JALR     = 5'b11001,
	OP_JAL      = 5'b11011,
	OP_SYSTEM   = 5'b11100
    } opcode_op_t;
    typedef union packed { // trivial union to avoid casts
	bit[ 4:0 ] raw;
	opcode_op_t op;
    } opcode_t;
    
    opcode_t OPCODE;
    assign OPCODE.raw = instruction[ 6:2 ];

    wire[ 4:0 ] RD;
    assign RD = instruction[ 11:7 ];

    wire[ 4:0 ] RS1;
    assign RS1 = instruction[ 19:15 ];
    
    wire[ 4:0 ] RS2;
    assign RS2 = instruction[ 24:20 ];
    
    typedef enum bit[ 2:0 ] {
	FUNC_LOAD_LB  = 3'b000,
	FUNC_LOAD_LH  = 3'b001,
	FUNC_LOAD_LW  = 3'b010,
	FUNC_LOAD_LBU = 3'b100,
	FUNC_LOAD_LHU = 3'b101
    } func_load_t;
    typedef enum bit[ 2:0 ] {
	FUNC_OP_ADD  = 3'b000, // SUB if reg/reg and bit 30 set
	FUNC_OP_SLL  = 3'b001,
	FUNC_OP_SLT  = 3'b010,
	FUNC_OP_SLTU = 3'b011,
	FUNC_OP_XOR  = 3'b100,
	FUNC_OP_SRL  = 3'b101, // SRA if bit 30 set
	FUNC_OP_OR   = 3'b110,
	FUNC_OP_AND  = 3'b111
    } func_op_t;
    typedef enum bit[ 2:0 ] {
	FUNC_STORE_SB  = 3'b000,
	FUNC_STORE_SH  = 3'b001,
	FUNC_STORE_SW  = 3'b010
    } func_store_t;
    typedef enum bit[ 2:0 ] {
	FUNC_COND_BEQ  = 3'b000,
	FUNC_COND_BNE  = 3'b001,
	FUNC_COND_BLT  = 3'b100,
	FUNC_COND_BGE  = 3'b101,
	FUNC_COND_BLTU = 3'b110,
	FUNC_COND_BGEU = 3'b111
    } func_cond_t;
    typedef enum bit[ 2:0 ] {
	FUNC_SYSTEM_ECALL = 3'b000
    } func_system_t;    
    typedef union packed {
	bit[ 2:0 ] raw;
	func_load_t load;
	func_op_t op;
	func_store_t store;
	func_cond_t cond;
	func_system_t system;
    } func_t;
    
    func_t FUNC;
    assign FUNC.raw = instruction[ 14:12 ];
    
    reg busreadmux;
    reg[ 31:0 ] bramread;
    reg[ 31:0 ] busread;
    reg[ 31:0 ] memread;
    reg[ 31:0 ] memwrite;
    reg[ 3:0 ] membyte;
    reg[ 31:2 ] memaddr;
    always @( posedge clk )
	if( new_cycle_type == CYCLE_WRITE ) begin
	    if( !memaddr[ 31 ] ) begin
		// write to internal memory
		if( membyte[ 0 ] )
		    mem[ memaddr[ 15:2 ] ][ 7:0 ] <= memwrite[ 7:0 ];
		if( membyte[ 1 ] )
		    mem[ memaddr[ 15:2 ] ][ 15:8 ] <= memwrite[ 15:8 ];
		if( membyte[ 2 ] )
		    mem[ memaddr[ 15:2 ] ][ 23:16 ] <= memwrite[ 23:16 ];
		if( membyte[ 3 ] )
		    mem[ memaddr[ 15:2 ] ][ 31:24 ] <= memwrite[ 31:24 ];
	    end
	    bramread <= 'x;
	end else
	    bramread <= mem[ memaddr[ 15:2 ] ];

    always_ff @( posedge clk ) begin
	busreadmux <= memaddr[ 31 ];
	busread <= rdata;
    end
    
    always_comb
	memread = busreadmux ? busread : bramread;
    
    function[ 31:0 ] sext12;
	input[ 11:0 ] val;

	sext12 = { {21{ val[ 11 ] } }, val[ 10:0 ] };
    endfunction
    
    reg[ 31:0 ] oper1;
    always_comb
	/* unique */ if( |RS1 )
	    oper1 = regs[ RS1 ];
	else
	    oper1 = 32'h00000000;
    
    reg[ 31:0 ] oper2;
    always_comb
	/* priority */ if( instruction[ 2 ] || !instruction[ 5 ] )
	    oper2 = sext12( instruction[ 31:20 ] );
	else if( |RS2 )
	    oper2 = regs[ RS2 ];
	else
	    oper2 = 32'h00000000;
	    
    reg[ 31:0 ] alu;
    always_comb 
	unique case( FUNC.op )
	FUNC_OP_ADD: // can also be SUB
	    alu = instruction[ 5 ] & !instruction[ 6 ] &
		  instruction[ 30 ] ? oper1 - oper2 : oper1 + oper2;
	FUNC_OP_SLL:
	    alu = oper1 << ( oper2[ 4:0 ] );
	FUNC_OP_SLT:
	    alu = { 31'b0, $signed( oper1 ) < $signed( oper2 ) };
	FUNC_OP_SLTU:
	    alu = { 31'b0, oper1 < oper2 };
	FUNC_OP_XOR:
	    alu = oper1 ^ oper2;
	FUNC_OP_SRL: // can also be SRA
	    if( instruction[ 30 ] )
		alu = $signed( oper1 ) >>> ( oper2[ 4:0 ] );
	    else
		alu = oper1 >> ( oper2[ 4:0 ] );
	FUNC_OP_OR:
	    alu = oper1 | oper2;
	FUNC_OP_AND:
	    alu = oper1 & oper2;
	default: // irrelevant unless FUNC undefined
	    alu = 'x;
	endcase
    
    reg cond;
    always_comb
	unique case( FUNC.cond )
	FUNC_COND_BEQ:
	    cond = oper1 == oper2;
	FUNC_COND_BNE:
	    cond = oper1 != oper2;
	FUNC_COND_BLT:
	    cond = $signed( oper1 ) < $signed( oper2 );	
	FUNC_COND_BGE:
	    cond = $signed( oper1 ) >= $signed( oper2 );	
	FUNC_COND_BLTU:
	    cond = oper1 < oper2;	
	FUNC_COND_BGEU:
	    cond = oper1 >= oper2;
	default:
	    cond = 'x;
	endcase      
    
    reg[ 31:0 ] byteaddr;
    always_comb
	unique case( new_cycle_type )
	CYCLE_READ:
	    byteaddr = oper1 + oper2;
	CYCLE_WRITE:
	    byteaddr = oper1 + sext12( { instruction[ 31:25 ],
					 instruction[ 11:7 ] } );
	default:
	    byteaddr = new_pc;
	endcase

    always_comb
	memaddr[ 31:2 ] = byteaddr[ 31:2 ];

    reg[ 1:0 ] byteoffset;
    always_ff @( posedge clk )
	byteoffset <= byteaddr[ 1:0 ];
    
    always_comb
	unique casez( instruction[ 13:12 ] )
	2'b00: // SB
	    memwrite = { 4{ oper2[ 7:0 ] } };
	2'b?1: // SH
	    memwrite = { 2{ oper2[ 15:0 ] } };
	2'b1?: // SW	    
	    memwrite = oper2;
	default:
	    memwrite = 'x; // irrelevant unless instruction undefined
	endcase
    
    always_comb
	unique casez( { instruction[ 13:12 ], byteaddr[ 1:0 ] } )
	4'b0000: // SB
	    membyte = 4'b0001;
	4'b0001: // SB
	    membyte = 4'b0010;
	4'b0010: // SB
	    membyte = 4'b0100;
	4'b0011: // SB
	    membyte = 4'b1000;
	4'b?10?: // SH
	    membyte = 4'b0011; // FIXME trap on unaligned
	4'b?11?: // SH
	    membyte = 4'b1100; // FIXME trap on unaligned
	4'b1???: // SW	    
	    membyte = 4'b1111; // FIXME trap on unaligned
	default:
	    membyte = 'x; // irrelevant unless instruction/addr undefined
	endcase
    
    always @( posedge clk )
	if( cycle_type == CYCLE_NORMAL ) begin
	    unique case( OPCODE )
	    OP_OP_IMM, OP_OP:
		regs[ RD ] <= alu;
	    OP_AUIPC:
		regs[ RD ] <= { instruction[ 31:12 ], 12'h000 } + pc;
	    OP_LUI:
		regs[ RD ] <= { instruction[ 31:12 ], 12'h000 };
	    OP_JAL, OP_JALR:
		regs[ RD ] <= pc + 4;
	    OP_SYSTEM:
		regs[ RD ] <= cycles; // RDCYCLE
	    default:
		;
	    endcase
	end else if( cycle_type == CYCLE_READ ) // FIXME trap on unaligned
	    unique case( FUNC.load )
	    FUNC_LOAD_LB:
		if( byteoffset[ 1:0 ] == 2'b00 )
		    regs[ RD ] <= { { 24{ memread[ 7 ] } }, 
				     memread[ 7:0 ] };
		else if( byteoffset[ 1:0 ] == 2'b01 )
		    regs[ RD ] <= { { 24{ memread[ 15 ] } }, 
				     memread[ 15:8 ] };
		else if( byteoffset[ 1:0 ] == 2'b10 )
		    regs[ RD ] <= { { 24{ memread[ 23 ] } }, 
				     memread[ 23:16 ] };
		else
		    regs[ RD ] <= { { 24{ memread[ 31 ] } }, 
				     memread[ 31:24 ] };
	    
	    FUNC_LOAD_LH:
		regs[ RD ] <= byteoffset[ 1 ] ?
			       { { 16{ memread[ 31 ] } }, memread[ 31:16 ] } :
			       { { 16{ memread[ 15 ] } }, memread[ 15:0 ] };
	    
	    FUNC_LOAD_LW:
		regs[ RD ] <= memread;
	    
	    FUNC_LOAD_LBU:
		if( byteoffset[ 1:0 ] == 2'b00 )
		    regs[ RD ] <= { 24'h000000, memread[ 7:0 ] };
		else if( byteoffset[ 1:0 ] == 2'b01 )
		    regs[ RD ] <= { 24'h000000, memread[ 15:8 ] };
		else if( byteoffset[ 1:0 ] == 2'b10 )
		    regs[ RD ] <= { 24'h000000, memread[ 23:16 ] };
		else
		    regs[ RD ] <= { 24'h000000, memread[ 31:24 ] };
	    
	    FUNC_LOAD_LHU:
		regs[ RD ] <= byteoffset[ 1 ] ?
			       { 16'h0000, memread[ 31:16 ] } :
			       { 16'h0000, memread[ 15:0 ] };
	    
	    default:
		regs[ RD ] <= 'x;
	    endcase
    
    reg[ 31:0 ] new_pc; // FIXME need only 31:2
    always_comb
	/* priority */ if( rst )
	    new_pc = 32'h00000000;
	else if( new_cycle_type == CYCLE_READ || new_cycle_type == CYCLE_WRITE )
	    new_pc = pc;
	else if( OPCODE == OP_JAL )
	    new_pc = pc + { { 12{ instruction[ 31 ] } }, instruction[ 19:12 ], 
			    instruction[ 20 ], instruction[ 30:21 ], 1'b0 };
	else if( OPCODE == OP_JALR )
	    new_pc = alu;
	else if( OPCODE == OP_BRANCH && cond )
	    new_pc = pc + ( sext12( { instruction[ 31 ], instruction[ 7 ], 
				      instruction[ 30:25 ], 
				      instruction[ 11:8 ] } ) << 1 );
	else
	    new_pc = pc + 4;
    
    always @( posedge clk )
	pc <= new_pc;

    always_comb
	unique case( cycle_type )
	CYCLE_NORMAL:
	    unique case( OPCODE )		   
	    OP_STORE:
		new_cycle_type = CYCLE_WRITE;
	    OP_LOAD:
		new_cycle_type = CYCLE_READ;
	    default:
		new_cycle_type = CYCLE_NORMAL;
	    endcase
	CYCLE_READ, CYCLE_WRITE:
	    new_cycle_type = CYCLE_NORMAL; // FIXME wait for ACK (though
	// writes can be queued)
	endcase

    always @( posedge clk )
	cycles <= cycles + 1'b1;

    assign addr[ 31:2 ] = memaddr[ 31:2 ];
    assign wdata = memwrite;
    assign wsel = membyte;
    assign we = new_cycle_type == CYCLE_WRITE;

`ifndef SYNTHESIS    
    always @( posedge clk )
	if( OPCODE == OP_SYSTEM && FUNC.system == FUNC_SYSTEM_ECALL )
	    if( regs[ 10 ] == 32'h73 ) begin
		$info( "Test success." );
		$finish();
	    end else
		$fatal( 2, "Test failure!" );
`endif
endmodule
