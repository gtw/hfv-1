module BB( input I, input T, output O, inout B );

    assign B = T ? 1'bz : I;
    assign O = B;
endmodule

module BBPU( input I, input T, output O, inout B );

    assign (pull1, pull0) B = 1'b1;
    assign B = T ? 1'bz : I;
    assign O = B;
endmodule

module DCCA( input CLKI, input CE, output CLKO );
    
    assign CLKO = CLKI & CE;
endmodule

module DTR( input STARTPULSE, output reg DTROUT7, output reg DTROUT6,
	    output reg DTROUT5, output reg DTROUT4, output reg DTROUT3,
	    output reg DTROUT2, output reg DTROUT1, output reg DTROUT0 );

    initial begin
	DTROUT0 = 1'bx;
	DTROUT1 = 1'bx;
	DTROUT2 = 1'bx;
	DTROUT3 = 1'bx;
	DTROUT4 = 1'bx;
	DTROUT5 = 1'bx;
	DTROUT6 = 1'b0;
	DTROUT7 = 1'b0;
    end	
    
    always @( negedge STARTPULSE ) begin
	DTROUT0 = 1'bx;
	DTROUT1 = 1'bx;
	DTROUT2 = 1'bx;
	DTROUT3 = 1'bx;
	DTROUT4 = 1'bx;
	DTROUT5 = 1'bx;
	DTROUT6 = 1'b0;
	DTROUT7 = 1'b0;	
	#70us;
	DTROUT0 = 1'b1;
	DTROUT1 = 1'b0;
	DTROUT2 = 1'b0;
	DTROUT3 = 1'b1;
	DTROUT4 = 1'b1;
	DTROUT5 = 1'b0;
	DTROUT6 = 1'b0;
	DTROUT7 = 1'b1;
    end	
endmodule

module EHXPLLL #( CLKI_DIV = 1, CLKFB_DIV = 1, CLKOP_DIV = 8,
		  CLKOS_DIV = 8, CLKOS2_DIV = 8, CLKOS3_DIV = 8,
		  CLKOP_ENABLE = "ENABLED", CLKOS_ENABLE = "DISABLED",
		  CLKOS2_ENABLE = "DISABLED",
		  CLKOS3_ENABLE = "DISABLED", CLKOP_CPHASE = 0,
		  CLKOS_CPHASE = 0, CLKOS2_CPHASE = 0,
		  CLKOS3_CPHASE = 0, CLKOP_FPHASE = 0, 
		  CLKOS_FPHASE = 0, CLKOS2_FPHASE = 0, 
		  CLKOS3_FPHASE = 0, FEEDBK_PATH = "CLKOP",
		  CLKOP_TRIM_POL = "RISING", CLKOP_TRIM_DELAY = 0,
		  CLKOS_TRIM_POL = "RISING", CLKOS_TRIM_DELAY = 0,
		  OUTDIVIDER_MUXA = "DIVA", OUTDIVIDER_MUXB = "DIVB",
		  OUTDIVIDER_MUXC = "DIVC", OUTDIVIDER_MUXD = "DIVD",
		  PLL_LOCK_MODE = 0, PLL_LOCK_DELAY = 200,
		  STDBY_ENABLE = "DISABLED", REFIN_RESET = "DISABLED",
		  SYNC_ENABLE = "DISABLED", 		  
		  INT_LOCK_STICKY = "ENABLED", 
		  DPHASE_SOURCE = "DISABLED", PLLRST_ENA = "DISABLED",
		  INTFB_WAKE = "DISABLED" )
    ( input CLKI, input CLKFB, input PHASESEL1 = 1'b0,      
      input PHASESEL0 = 1'b0, input PHASEDIR = 1'b0, 
      input PHASESTEP = 1'b0, input PHASELOADREG = 1'b0, 
      input STDBY = 1'b0, input PLLWAKESYNC = 1'b0, input RST = 1'b0,
      input ENCLKOP = 1'b1, input ENCLKOS = 1'b1, 
      input ENCLKOS2 = 1'b1, input ENCLKOS3 = 1'b1, 
      output CLKOP, output CLKOS, output CLKOS2, output CLKOS3,
      output LOCK, output INTLOCK, output REFCLK, output CLKINTFB );
    // don't even try to model
endmodule

module GSR( input GSR );
endmodule

module ODDRX1F( input SCLK, input RST, input D0, input D1, output reg Q );

    always_ff @( edge SCLK )
	Q <= SCLK ? D0 : D1;
endmodule

module OSCG( output reg OSC );

    initial OSC = 1'b0;

    always #1 OSC = !OSC;
endmodule
